sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./utilities",
	"sap/m/List",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/StandardListItem",
	"sap/ui/core/routing/History",
	"com/sap/build/standard/twence1/model/formatter",
	"jquery.sap.global",
	"jquery.sap.script",
	"sap/m/Dialog",
	"sap/m/Button",
	"sap/m/Link"
], function(Controller, MessageBox, Utilities, List, Filter, FilterOperator, StandardListItem, History, formatter, jQuery, $sap, Dialog,
	Button, Link) {
	"use strict";
	return Controller.extend("com.sap.build.standard.twence1.controller.Transport", {
		formatter: formatter,
		handleRouteMatched: function(oEvent) {
			var oParams = {};
			if (oEvent.mParameters.data.context) {
				this.sContext = oEvent.mParameters.data.context;
				var oPath;
				if (this.sContext) {
					oPath = {
						path: "/" + this.sContext,
						parameters: oParams
					};
					this.getView().bindObject(oPath);
				}
			}
		},
		onDialogPress: function () {
			if (!this.pressDialog) {
				this.pressDialog = new Dialog({
					title: 'Belangrijke Telefoonnummers',
					content:  [
									new sap.m.Text({
										text : "\\n\Bij storing       : Helpdesk ICT            -   074 2404 455\\n\\n\Emmacase      : Binnendienst Nood     -  074 2404 567\\n\\n\CCR               : Teamleider                -  074 2404 555\\n\\n\Weegbrug      : Binnenkomend            - 074 2404 470\\n\\n\Weegbrug      : Administratie              -  074 2404 477\\n\\n\M & S            : Paul Burgers               -  074 2404 593\\n\\n\Wilfried Oude Reimer  -  074 2404 568"
									})
								],
					beginButton: new Button({
						text: 'Close',
						press: function () {
							this.pressDialog.close();
						}.bind(this)
					})
				});
				this.getView().addDependent(this.pressDialog);
			}
			this.pressDialog.open();
		},
		_onImagePress2: function(oEvent) {
			var oBindingContext = oEvent.getSource().getBindingContext();
			return new Promise(function(fnResolve) {
				this.boNavigate("Wdf", oBindingContext, fnResolve, "");
							//dindow.location.reload();
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});
		},
		onRefreshPress: function() {
			window.location.reload();
		},
		boNavigate: function(sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = (oBindingContext) ? oBindingContext.getModel() : null;
			var sEntityNameSet;
			if (sPath !== null && sPath !== "") {
				if (sPath.substring(0, 1) === "/") {
					sPath = sPath.substring(1);
				}
				sEntityNameSet = sPath.split("(")[0];
			}
			var sNavigationPropertyName;
			var sMasterContext = this.sMasterContext ? this.sMasterContext : sPath;
			if (sEntityNameSet !== null) {
				sNavigationPropertyName = sViaRelation || this.getOwnerComponent().getNavigationPropertyForNavigationWithContext(sEntityNameSet,
					sRouteName);
			}
			if (sNavigationPropertyName !== null && sNavigationPropertyName !== undefined) {
				if (sNavigationPropertyName === "") {
					this.oRouter.navTo(sRouteName, {
						context: sPath,
						masterContext: sMasterContext
					}, false);
				} else {
					oModel.createBindingContext(sNavigationPropertyName, oBindingContext, null, function(bindingContext) {
						if (bindingContext) {
							sPath = bindingContext.getPath();
							if (sPath.substring(0, 1) === "/") {
								sPath = sPath.substring(1);
							}
						} else {
							sPath = "undefined";
						}
						if (sPath === "undefined") {
							this.oRouter.navTo(sRouteName);
						} else {
							this.oRouter.navTo(sRouteName, {
								context: sPath,
								masterContext: sMasterContext
							}, false);
						}
					}.bind(this));
				}
			} else {
				this.oRouter.navTo(sRouteName);
			}
			if (typeof fnPromiseResolve === "function") {
				fnPromiseResolve();
			}
		},
			doNavigate: function(sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = (oBindingContext) ? oBindingContext.getModel() : null;
			var fPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var sLock = fPath = oBindingContext.getProperty("Mark");
			if (sLock === "X") {
				MessageBox.error("Deze is gelocked");
				return;
			} else {
				var sEntityNameSet;
				if (sPath !== null && sPath !== "") {
					if (sPath.substring(0, 1) === "/") {
						sPath = sPath.substring(1);
					}
					sEntityNameSet = sPath.split("(")[0];
				}
				var sNavigationPropertyName;
				var sMasterContext = this.sMasterContext ? this.sMasterContext : sPath;
			}
			if (sEntityNameSet !== null) {
				sNavigationPropertyName = "VoertuigenSet"; 
			}
			if (sNavigationPropertyName !== null && sNavigationPropertyName !== undefined) {
				if (sNavigationPropertyName === "") {
					this.oRouter.navTo(sRouteName, {
						context: sPath,
						masterContext: sMasterContext
					}, false);
				} else {
					sNavigationPropertyName = "";
					oModel.createBindingContext(sNavigationPropertyName, oBindingContext, null, function(bindingContext) {
						if (bindingContext) {
							sPath = bindingContext.getPath();
							if (sPath.substring(0, 1) === "/") {
								sPath = sPath.substring(1);
							}
						} else {
							sPath = "undefined";
						}
						if (sPath === "undefined") {
							this.oRouter.navTo(sRouteName);
							$.ajax({
								url: '/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/LockTransSet',
								type: 'POST',
								data: {
									Origin: "Origin",
									IdSap: "IdSap",
									Voorach: "Voorach"
								},
								processData: false,
								success: function(data) {
									console.log("success" + data);
								},
								error: function(e) {
									console.log("error: " + e);
								}
							});
						} else {
							var oEntry = {};
							oEntry.Origin = oBindingContext.getProperty("Origin");
							oEntry.IdSap = oBindingContext.getProperty("IdSap");
							oEntry.Voorach = oBindingContext.getProperty("Voorach");
							oModel.create("/LockTransSet", oEntry, {
								method: "POST",
								success: function(data) {
									//					alert("success");
								},
								error: function(e) {
									alert("error");
								}
							});
							this.oRouter.navTo(sRouteName, {
								context: sPath,
								masterContext: sMasterContext
							}, false);
						}
					}.bind(this));
				}
			} else {
				this.oRouter.navTo(sRouteName);
			}
			if (typeof fnPromiseResolve === "function") {
				fnPromiseResolve();
			}
		},
			onFilterSearch: function(oEvent) {
			var sQuery = oEvent.getParameter('query');
			path: '/VoertuigenSet' [
				"Name1",
				"Lgpla",
				"Wdf",
				"Soort",
				"InspectieText"
			].forEach(function(sListId) {
				var _fnTestContains = function(

					/* String */
					sQuery /* Search string */ ,
					/* Boolean */
					bCaseSensitive /* Is the search is case sentive or not */ ,
					/* String */
					sTestString /* String to text */
				) {
					return (bCaseSensitive ? sTestString : sTestString.toUpperCase()).indexOf(bCaseSensitive ? sQuery : sQuery.toUpperCase()) > -1;
				};
				var oPath = this.getView().byId(sListId);
				if (oPath) {
					var oBinding = oPath.getBinding("items");
					if (oBinding) {
						if (sQuery) {
							oBinding.filter(new Filter({
								filters: [
									new Filter("StatusTxt", FilterOperator.Contains, sQuery),
									new Filter("Name1", FilterOperator.Contains, sQuery),
									new Filter("Lgpla", FilterOperator.Contains, sQuery),
									new Filter("Wdf", FilterOperator.Contains, sQuery),
									new Filter("Soort", FilterOperator.Contains, sQuery),
									new Filter("InspectieText", FilterOperator.Contains, sQuery)
								],
								and: false
							}));
						} else {
							oBinding.filter([]);
						}
					}
				}
			}.bind(this));
		},
		onNavBack: function(oEvent) {
				var oBindingContext = oEvent.getSource().getBindingContext();
			return new Promise(function(fnResolve) {
				this.boNavigate("Wdf", oBindingContext, fnResolve, "");
							//dindow.location.reload();
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});
		},
			_showuser: 	function(ShowUsername) {
		var name = this.getView().byId("WDF_select").getItems()[0].getBindingContext().getProperty("Controleur");
		var welkom = "U bent ingelogt als gebruiker:";
		var UserText = welkom+name;
		var authtic = "Basic 1324654987984567897464654"+btoa+username;
		var close = "close";
		var username = "(USERNAME +  + PASSWORD,+ )";
				var oShell = new sap.m.Shell();
				var dialog = new Dialog({
				title: 'User',
				type: 'Message',
				
				content: new sap.m.Text({ text : UserText }),
				beginButton: new Button({
					text: 'logout',
					press: function () {
						oShell.destroyApp();
					    sap.ui.getCore().applyChanges();
					    	$.ajax({
								url: '/sap/opu/odata/sap/public/bc/icf/logoff',
								type: 'GET',
								headers: {
								Authorization: authtic + btoa(username),
								Connection: close, username: "COPS_USER", password: "init123"},
								 data: {},
								processData: true,
								success: function(data) {
									console.log("success" + data);
								},
								error: function(e) {
									console.log("error: " + e);
								}
							});
					    jQuery(document.body).html("<span>Logged out successfully.</span>");
						window.open('','_self').close();
						dialog.close();
					}
				}),
				endButton: new Button({
					text: 'Ok',
					press: function () {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});

			dialog.open();
	/*	var text = this.getView().byId("username");
		text.setWrapping(!text.getWrapping());
		this.getView().byId("username").setText(text.getWrapping() ? name : name);*/
		//var sId = getView().byId("username");  
	//	sap.ui.getCore().byId(sId).setValue(oSelectedItem.getTitle());
	//	sap.ui.getCore().byId("username");
	//	sap.m.Text.setText(name);
		//this.getId(sId).setText(name);
		
	},
		_onTableItemPress: function(oEvent) {
			var oBindingContext = oEvent.getParameter("listItem").getBindingContext();
			return new Promise(function(fnResolve) {
				this.doNavigate("Details", oBindingContext, fnResolve, "");
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});
		},
		onInit: function() {
			this.mBindingOptions = {};
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("Transport").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
		},
		_showEmma: function(oEvent, sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {
					var oBindingContext = oEvent.getSource().getBindingContext();
					var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
					var eMmaCase = sPath = oBindingContext.getProperty("EmmaText");
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.information(
				eMmaCase,
				{
					styleClass: bCompact ? "sapUiSizeCompact" : ""
				}
			);
		}
	});
}, /* bExport= */ true);