sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./utilities",
	"sap/ui/core/routing/History",
	"sap/m/Dialog",
	"sap/m/Button",
	"com/sap/build/standard/twence1/model/formatter"
], function(BaseController, MessageBox, Utilities, History, Dialog, Button, formatter) {
	"use strict";

	return BaseController.extend("com.sap.build.standard.twence1.controller.Details", {
		formatter: formatter,

		oEntry: {},
		oKentekenTest: "",

		handleRouteMatched: function(oEvent, sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {

			var oParams = {};

			if (oEvent.mParameters.data.context) {
				this.sContext = oEvent.mParameters.data.context;
				var oPath;
				if (this.sContext) {
					oPath = {
						path: "/" + this.sContext,
						parameters: oParams
					};
					this.getView().bindObject(oPath);

					var oItem = this.getView();
					var model = oItem.getModel();
					var dataContext = model.getProperty("/" + this.sContext);

					//		onAfterRendering: function() {
					var oModel1 = new sap.ui.model.json.JSONModel();
					var oModel2 = new sap.ui.model.json.JSONModel();
					var oModel3 = new sap.ui.model.json.JSONModel();
					var oModel4 = new sap.ui.model.json.JSONModel();
					var oModelEmma = new sap.ui.model.json.JSONModel();
					var oTreeModel = new sap.ui.model.json.JSONModel();
					var data1;
					var data2;
					var data3;
					var data4;
					var dataEmma;
					var dataTree;

					var sRoot = "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/RejectReasonsFlatSet?$filter=" + "Wdf eq '" + dataContext['Wdf'] + "'";

					$.ajax({
						type: "GET",
						url: sRoot,
						dataType: "json",
						success: function(dataTree, textStatus, jqXHR) {
							//console.log(Data);
							sap.ui.getCore().setModel(oTreeModel, "jsonTreeModel");
							oTreeModel.setData({
								tree: dataTree
							});

							//  sap.m.MessageToast.show("OK");
							//After you get the data you can set the data to any field as required.
						},
						error: function(jqXHR, textStatus, errorThrown) {
							sap.m.MessageToast.show("NOT OK");
						}
					});

					sap.ui.getCore().setModel(oTreeModel, "jsonTreeModel");
					oTreeModel.setData({
						tree: dataTree
					});

					sap.ui.getCore().setModel(oModelEmma, "jsonModelEmma");
					oModelEmma.setData({
						fotolijst: dataEmma
					});

					sRoot = "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/FotolijstSet?$filter=CasenrInsp eq '";
					var sUrl = sRoot.concat(dataContext['CasenrInsp'], "'");

					$.ajax({
						type: "GET",
						url: sUrl,
						dataType: "json",
						success: function(dataEmma, textStatus, jqXHR) {
							//console.log(Data);
							sap.ui.getCore().setModel(oModelEmma, "jsonModelEmma");
							oModelEmma.setData({
								fotolijst: dataEmma
							});

							//	sap.m.MessageToast.show("OK");
							//After you get the data you can set the data to any field as required.
						},
						error: function(jqXHR, textStatus, errorThrown) {
							sap.m.MessageToast.show("NOT OK");
						}
					});
					sap.ui.getCore().setModel(oModel1, "jsonModel1");
					oModel1.setData({
						kentekens: data1
					});

					sRoot = "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/KentekensSet?$filter=Voorach eq '";
					var sUrl = sRoot.concat(dataContext['Voorach'], "'");

					$.ajax({
						type: "GET",
						url: sUrl,
						dataType: "json",
						success: function(data1, textStatus, jqXHR) {
							//console.log(Data);
							sap.ui.getCore().setModel(oModel1, "jsonModel1");
							oModel1.setData({
								kentekens: data1
							});
						},
						error: function(jqXHR, textStatus, errorThrown) {
							sap.m.MessageToast.show("NOT OK");
						}
					});

					sap.ui.getCore().setModel(oModel2, "jsonModel2");
					oModel2.setData({
						locaties: data2
					});

					sRoot = "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/LocatiesSet?$filter=IdSap eq '";
					sUrl = sRoot.concat(dataContext['IdSap'], "' and Origin eq '", dataContext['Origin'], "' and Wdf eq '", dataContext['Wdf'],
						"' and Processtype eq '", dataContext['Processtype'], "' and  Waste eq '", dataContext['Waste'], "' and Lgnum eq '",
						dataContext['Storagenum'], "' and Lgtyp eq '", dataContext['Lgtyp'], "' and Voorach eq '", dataContext['Voorach'], "'");

					$.ajax({
						type: "GET",
						url: sUrl,
						dataType: "json",
						success: function(data2, textStatus, jqXHR) {
							//console.log(Data);
							sap.ui.getCore().setModel(oModel2, "jsonModel2");
							oModel2.setData({
								locaties: data2
							});

							oModel2.setData(oModel1.getData(), true);
							//	sap.m.MessageToast.show("OK");
							//After you get the data you can set the data to any field as required.
						},
						error: function(jqXHR, textStatus, errorThrown) {
							sap.m.MessageToast.show("NOT OK");
						}
					});

					sap.ui.getCore().setModel(oModel3, "jsonModel3");
					oModel3.setData({
						artikelen: data3
					});

					sRoot = "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/ArtikelenSet?$filter=Vbeln eq '";
					sUrl = sRoot.concat(dataContext['Exti1'], "' and Atwrt eq '", dataContext['Legaldocument'], "'");

					$.ajax({
						type: "GET",
						url: sUrl,
						dataType: "json",
						success: function(data3, textStatus, jqXHR) {
							//console.log(Data);
							sap.ui.getCore().setModel(oModel3, "jsonModel3");
							oModel3.setData({
								artikelen: data3
							});
							oModel1.setData(oModel3.getData(), true);
							//	sap.m.MessageToast.show("OK");
							//After you get the data you can set the data to any field as required.
						},
						error: function(jqXHR, textStatus, errorThrown) {
							sap.m.MessageToast.show("NOT OK");
						}
					});

					sap.ui.getCore().setModel(oModel4, "jsonModel4");
					oModel4.setData({
						containers: data4
					});

					var sUrl = "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/ContainersSet";
					//					var sUrl = sRoot.concat(dataContext['Voorach'], "'");
					$.ajax({
						type: "GET",
						url: sUrl,
						dataType: "json",
						success: function(data4, textStatus, jqXHR) {
							//console.log(Data);
							sap.ui.getCore().setModel(oModel4, "jsonModel4");
							oModel4.setData({
								containers: data4
							});

							//	sap.m.MessageToast.show("OK");
							//After you get the data you can set the data to any field as required.
						},
						error: function(jqXHR, textStatus, errorThrown) {
							sap.m.MessageToast.show("NOT OK");
						}
					});

					//					var oModel = new sap.ui.model.json.JSONModel();

					//					oModel2.setData(oModel3.getData(), true);
					//					oModel1.setData(oModel2.getData(), true);
					//					oModel.setData(oModel1.getData(), true);
					//					sap.ui.getCore().setModel(oModel, "jsonModel");				

				}
			}

			var that = this;
			that._lockThisTransport;

		},

		_checkKentekena: function(kenteken) {

			//var sQuery = "06BPHP";
			var sQuery = this.oKentekenTest;
			return kenteken === sQuery;

		},

		checkKenteken: function(oEvent) {
			var oView = this.getView();
			//			var sControlId = "Kentekenset";
			//			var oControl = this.getView().byId(sControlId);
			var sQuery = oView.byId("kenteken").getValue();
			this.oKentekenTest = sQuery;
			var sSourceId = sap.ui.getCore().getModel("jsonModel1");
			var array = sSourceId.oData.kentekens.d.results;
			var kentekenarray = array.filter(function(kenteken) {
				return kenteken.Kenteken === sQuery;
			});
			//			console.log(kentekenarray);
			if (kentekenarray.length <= 0) {
				sap.m.MessageToast.show("Kenteken is niet correct");
			}

			// true: function(data) {
			// 				alert("success");
			// },
			// false: function(e) {
			// 		alert("error");
			// }

			//sSourceId.filter(function(kenteken){return kenteken = sQuery;})
		},
		updateBindingOptions: function(sCollectionId, oBindingData, sSourceId) {
			this.mBindingOptions[sCollectionId] = this.mBindingOptions[sCollectionId] || {};

			var aSorters = oBindingData.sorters === undefined ? this.mBindingOptions[sCollectionId].sorters : oBindingData.sorters;
			var oGroupby = oBindingData.groupby === undefined ? this.mBindingOptions[sCollectionId].groupby : oBindingData.groupby;

			// 1) Update the filters map for the given collection and source
			this.mBindingOptions[sCollectionId].sorters = aSorters;
			this.mBindingOptions[sCollectionId].groupby = oGroupby;
			this.mBindingOptions[sCollectionId].filters = this.mBindingOptions[sCollectionId].filters || {};
			this.mBindingOptions[sCollectionId].filters[sSourceId] = oBindingData.filters || [];

			// 2) Reapply all the filters and sorters
			var aFilters = [];
			for (var key in this.mBindingOptions[sCollectionId].filters) {
				aFilters = aFilters.concat(this.mBindingOptions[sCollectionId].filters[key]);
			}

			// Add the groupby first in the sorters array
			if (oGroupby) {
				aSorters = aSorters ? [oGroupby].concat(aSorters) : [oGroupby];
			}

			var aFinalFilters = aFilters.length > 0 ? [new sap.ui.model.Filter(aFilters, true)] : undefined;
			return {
				filters: aFinalFilters,
				sorters: aSorters
			};

		},
		_updateSet: function(oEvent, sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {
			var oView = this.getView();
			var oBindingContext = oEvent.getSource().getBindingContext();

			var oEntry = {};
			oEntry.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
			oEntry.Objnr = oBindingContext.getProperty("Objnr");
			oEntry.ObjectGrp = oBindingContext.getProperty("ObjectGrp");
			oEntry.FleetCat = oBindingContext.getProperty("FleetCat");
			oEntry.Voorach = oBindingContext.getProperty("Voorach");
			oEntry.Origin = oBindingContext.getProperty("Origin");
			oEntry.IdSap = oBindingContext.getProperty("IdSap");
			oEntry.Waste = oBindingContext.getProperty("Waste");
			oEntry.TransnrI = oBindingContext.getProperty("TransnrI");
			oEntry.WasteDesc = oBindingContext.getProperty("WasteDesc");
			oEntry.Charg = oBindingContext.getProperty("Charg");
			oEntry.Verif = oBindingContext.getProperty("Verif");
			oEntry.WdfDescription = oBindingContext.getProperty("WdfDescription");
			oEntry.LicenseNum = oBindingContext.getProperty("LicenseNum");
			oEntry.ContNumber = oBindingContext.getProperty("ContNumber");
			oEntry.Lbert = oView.byId("locatietekst").getText();
			oEntry.Lgpla = oBindingContext.getProperty("Lgpla");
			oEntry.ContractPosnr = oBindingContext.getProperty("ContractPosnr");

			var oEntry2 = {};
			oEntry2.ContNumber = oBindingContext.getProperty("ContNumber");
			oEntry2.Soort = oBindingContext.getProperty("Soort");
			oEntry2.Contract = oBindingContext.getProperty("Contract");
			oEntry2.Transporter = oBindingContext.getProperty("Transporter");
			oEntry2.Waste = oBindingContext.getProperty("Waste");
			oEntry2.Lgtyp = oBindingContext.getProperty("Lgtyp");
			oEntry2.Voorach = oBindingContext.getProperty("Voorach");
			oEntry2.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
			oEntry2.Objnr = oBindingContext.getProperty("Objnr");
			oEntry2.ObjectGrp = oBindingContext.getProperty("ObjectGrp");
			oEntry2.FleetCat = oBindingContext.getProperty("FleetCat");
			oEntry2.LicenseNum = oBindingContext.getProperty("LicenseNum");

			var oDetails = {};
			oDetails.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
			oDetails.Origin = oView.byId("origin").getText();
			oDetails.IdSap = oView.byId("idsap").getText();
			oDetails.Voorach = oView.byId("voorach").getText();
			oDetails.Signi = oView.byId("kenteken").getValue();
			oDetails.Lgpla = oView.byId("locatie").getValue();
			oDetails.Waste = oView.byId("waste").getValue();
			oDetails.ContNumber = oView.byId("container").getValue();
			oDetails.WasteDesc = oView.byId("wastedesc").getText();
			oDetails.Lbert = oView.byId("locatietekst").getText();
			oDetails.Name1 = oView.byId("transporteur").getText();
			oDetails.Milieuclass = oView.byId("mtype").getUnit();
			oDetails.Charg = oView.byId("socharge").getText();

			var oModel = this.getOwnerComponent().getModel();
			oModel.update("/UpdateDetailsSet(Origin='" + oDetails.Origin + "',IdSap='" + oDetails.IdSap + "')", oDetails, {
				method: "PUT",
				success: function(data) {},
				error: function(e) {
					// lert("error");
				}
			});

			var sRoot = "SaveEndSet(Wdplantnr='" + oEntry.Wdplantnr + "',Objnr='" + oEntry.Objnr + "',ObjectGrp='" + oEntry.ObjectGrp +
				"',FleetCat='" + oEntry.FleetCat + "',Voorach='" + oEntry.Voorach + "',Origin='" + oEntry.Origin + "',IdSap='" + oEntry.IdSap +
				"',Waste='" + oEntry.Waste + "',TransnrI='" + oEntry.TransnrI + "',WasteDesc='" + oEntry.WasteDesc + "',Charg='" + oEntry.Charg +
				"',Verif='" + oEntry.Verif + "',WdfDescription='" + oEntry.WdfDescription + "',LicenseNum='" + oEntry.LicenseNum + "',Lbert='" +
				oEntry.Lbert + "',Lgpla='" + oBindingContext.getProperty("Lgpla") + "',ContNumber='" + oBindingContext.getProperty("ContNumber") +
				"',ContractPosnr='" + oEntry.ContractPosnr + "',Reason='" + oBindingContext.getProperty("InspectieText") + "')";
			var sURL = "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/" + sRoot;

			while (sRoot.search("/") !== -1) {
				sRoot = sRoot.replace("/", "%2F");
			}

			sURL = "/" + sRoot;

			var oModelOdata = this.getOwnerComponent().getModel();

			oModelOdata.setUseBatch(false);

			oModelOdata.update(sURL, oEntry, {
				method: "PUT",
				success: function() {
					//	sap.ui.commons.MessageBox.show(
					//		sap.ui.commons.MessageBox.alert("Success!")
					//		);
				},
				error: function() {
					//	sap.ui.commons.MessageBox.alert("Error!");
				}
			});

			return new Promise(function(fnResolve) {

				this.doNavigate("Transport", oBindingContext, fnResolve, "");
				window.location.reload();
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					//	MessageBox.error(err.message);
				}
			});

		},

		_lockThisTransport: function(oEvent, sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {
			var oItem = this.getView();
			var model = oItem.getModel();
			var dataContext = model.getProperty("/" + this.sContext);
			var oEntry = {};
			oEntry.Origin = dataContext.Origin;
			oEntry.IdSap = dataContext.IdSap;
			oEntry.Voorach = dataContext.Voorach;
			//	var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = new sap.ui.model.odata.v2.ODataModel();
			oModel.create("/LockTransSet", oEntry, {
				method: "POST",
				success: function(data) {
					alert("success");
				},
				error: function(e) {
					alert("error");
				}
			});

			/*
			});
		},
			_TruckCheck: function() {
			var sTruckCheck = this.getId("DetailsForm").getContent();

			var sError = false;

			sTruckCheck.forEach(function(Field) {
				if (typeof Field.getValue === "function") {

					if (!Field.getValue() || Field.getValue().length < 1) {
						Field.setValueState("Error");

						sError = true;

					}
					else {
						Field.setValueState("None");
					}

				}

			});
			return sError;

		},
		_showuser: 	function(ShowUsername) {
		var name = this.getView().byId("WDF_select").getItems()[0].getBindingContext().getProperty("Controleur");
		var welkom = "U bent ingelogt als gebruiker:";
		var UserText = welkom+name;
		var authtic = "Basic 1324654987984567897464654"+btoa+username;
		var close = "close";
		var username = "(USERNAME +  + PASSWORD,+ )";
				var oShell = new sap.m.Shell();
				var dialog = new Dialog({
				title: 'User',
				type: 'Message',
				
				content: new sap.m.Text({ text : UserText }),
				beginButton: new Button({
					text: 'logout',
					press: function () {
						oShell.destroyApp();
					    sap.ui.getCore().applyChanges();
					    	$.ajax({
								url: '/sap/opu/odata/sap/public/bc/icf/logoff',
								type: 'GET',
								headers: {
								Authorization: authtic + btoa(username),
								Connection: close, username: "COPS_USER", password: "init123"},
								 data: {},
								processData: true,
								success: function(data) {
									console.log("success" + data);
								},
								error: function(e) {
									console.log("error: " + e);
								}
							});
					    jQuery(document.body).html("<span>Logged out successfully.</span>");
						dialog.close();
					}
				}),
				endButton: new Button({
					text: 'Ok',
					press: function () {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});

			dialog.open();
	/*	var text = this.getView().byId("username");
		text.setWrapping(!text.getWrapping());
		this.getView().byId("username").setText(text.getWrapping() ? name : name);*/
			//var sId = getView().byId("username");  
			//	sap.ui.getCore().byId(sId).setValue(oSelectedItem.getTitle());
			//	sap.ui.getCore().byId("username");
			//	sap.m.Text.setText(name);
			//this.getId(sId).setText(name);

		},
		_CreateEMMACase: function(oProfileID) {
			this.getModel().setProperty("/Emma(" + this.EMMACASE.emmaID + ")/Emma", oProfileID);
			this.getModel().submitChanges();
		},

		_onomleidingPress: function(oEvent, sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {
			var oParams = {};
			var oView = this.getView();
			var oBindingContext = oEvent.getSource().getBindingContext();
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = (oBindingContext) ? oBindingContext.getModel() : null;
			var oEntry = {};
			oEntry.Origin = oBindingContext.getProperty("Origin");
			oEntry.IdSap = oBindingContext.getProperty("IdSap");
			oEntry.Voorach = oBindingContext.getProperty("Voorach");
			oEntry.Legaldocument = oBindingContext.getProperty("Legaldocument");
			oEntry.Transnr = oBindingContext.getProperty("Transnr");
			oEntry.Licenseplate = oView.byId("kenteken").getValue();
			oModel.setUseBatch(false);
			var that = this;
			oModel.update("/NegeerOmleidingSet(Origin='" + oEntry.Origin +
				"',IdSap='" + oEntry.IdSap +
				"',Voorach='" + oEntry.Voorach +
				"',Legaldocument='" + oEntry.Legaldocument +
				"',Transnr='" + oEntry.Transnr +
				"',Licenseplate='" + oEntry.Licenseplate +
				"')", oEntry, {
					method: "PUT",
					success: function(data) {
						return new Promise(function(fnResolve) {
							that.doNavigate("Transport", oBindingContext, fnResolve, "");
							window.location.reload();
						}.bind(this)).catch(function(err) {
							if (err !== undefined) {
								MessageBox.error(err.message);
							}
						});
					},
					error: function(e) {
						//                          alert("error");
					}

				});

			return new Promise(function(fnResolve) {
				/*   this.doNavigate("Transport", oBindingContext, fnResolve, "");
				   window.location.reload();*/
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});
		},

		_onImagePress: function(oEvent) {
			var oBindingContext = oEvent.getSource().getBindingContext();
			return new Promise(function(fnResolve) {

				this.doNavigate("Wdf", oBindingContext, fnResolve, "");
				window.location.reload();
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});
		},
		doNavigate: function(sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {

			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = (oBindingContext) ? oBindingContext.getModel() : null;

			var sEntityNameSet;
			if (sPath !== null && sPath !== "") {
				if (sPath.substring(0, 1) === "/") {
					sPath = sPath.substring(1);
				}
				sEntityNameSet = sPath.split("(")[0];
			}
			var sNavigationPropertyName;
			var sMasterContext = this.sMasterContext ? this.sMasterContext : sPath;

			if (sEntityNameSet !== null) {
				sNavigationPropertyName = sViaRelation || this.getOwnerComponent().getNavigationPropertyForNavigationWithContext(sEntityNameSet,
					sRouteName);
			}
			if (sNavigationPropertyName !== null && sNavigationPropertyName !== undefined) {
				if (sNavigationPropertyName === "") {
					this.oRouter.navTo(sRouteName, {
						context: sPath,
						masterContext: sMasterContext
					}, false);
				} else {
					oModel.createBindingContext(sNavigationPropertyName, oBindingContext, null, function(bindingContext) {
						if (bindingContext) {
							sPath = bindingContext.getPath();
							if (sPath.substring(0, 1) === "/") {
								sPath = sPath.substring(1);
							}
						} else {
							sPath = "undefined";
						}

						// If the navigation is a 1-n, sPath would be "undefined" as this is not supported in Build
						if (sPath === "undefined") {
							this.oRouter.navTo(sRouteName);
						} else {
							this.oRouter.navTo(sRouteName, {
								context: sPath,
								masterContext: sMasterContext
							}, false);
						}
					}.bind(this));
				}
			} else {
				this.oRouter.navTo(sRouteName);
			}

			if (typeof fnPromiseResolve === "function") {
				fnPromiseResolve();
			}
		},
		_onImagePress1: function(oEvent) {

			var oBindingContext = oEvent.getSource().getBindingContext();

			return new Promise(function(fnResolve) {

				this.doNavigate("Transport", oBindingContext, fnResolve, "");
				window.location.reload();
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});

		},
		_onInputValueHelpRequest: function(oEvent, oBindingContext) {
			/*		if (this._LastCheck()) {
							return;
						}*/

			var oEntry = {};
			var sDialogName = "Dialog1";
			this.mDialogs = this.mDialogs || {};
			var oDialog = this.mDialogs[sDialogName];
			var oSource = oEvent.getSource();
			var oBindingContext = oSource.getBindingContext();
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oView;
			if (!oDialog) {
				this.getOwnerComponent().runAsOwner(function() {
					oView = sap.ui.xmlview({
						viewName: "com.sap.build.standard.twence1.view." + sDialogName
					});
					this.getView().addDependent(oView);
					oView.getController().setRouter(this.oRouter);
					oDialog = oView.getContent()[0];
					this.mDialogs[sDialogName] = oDialog;
				}.bind(this));
			}

			return new Promise(function(fnResolve) {
				oDialog.attachEventOnce("afterOpen", null, fnResolve);
				oDialog.open();
				if (oView) {
					oDialog.attachAfterOpen(function() {
						//	oDialog.rerender();
					});
				} else {
					oView = oDialog.getParent();
				}

				//				var oModel = this.getView().getModel();
				var oModel = sap.ui.getCore().getModel("jsonModel1");
				if (oModel) {
					oView.setModel(oModel);
				}
				if (sPath) {
					var oParams = oView.getController().getBindingParameters();
					oView.bindObject({
						path: sPath,
						parameters: oParams
					});
				}
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});

		},
		_onInputValueHelpRequest1: function(oEvent) {

			var sDialogName = "Dialog4";
			this.mDialogs = this.mDialogs || {};
			var oDialog = this.mDialogs[sDialogName];
			var oSource = oEvent.getSource();
			var oBindingContext = oSource.getBindingContext();
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oView;
			if (!oDialog) {
				this.getOwnerComponent().runAsOwner(function() {
					oView = sap.ui.xmlview({
						viewName: "com.sap.build.standard.twence1.view." + sDialogName
					});
					this.getView().addDependent(oView);
					oView.getController().setRouter(this.oRouter);
					oDialog = oView.getContent()[0];
					this.mDialogs[sDialogName] = oDialog;
				}.bind(this));
			}

			return new Promise(function(fnResolve) {
				oDialog.attachEventOnce("afterOpen", null, fnResolve);
				oDialog.open();
				if (oView) {
					oDialog.attachAfterOpen(function() {
						//	oDialog.rerender();
					});
				} else {
					oView = oDialog.getParent();
				}

				//				var oModel = this.getView().getModel();

				var oModel = sap.ui.getCore().getModel("jsonModel4");
				//			this.getView().setModel(oModel);
				if (oModel) {
					oView.setModel(oModel);
				}
				if (sPath) {
					var oParams = oView.getController().getBindingParameters();
					oView.bindObject({
						path: sPath,
						parameters: oParams
					});
				}
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});

		},
		_onInputValueHelpRequest2: function(oEvent) {

			var sDialogName = "Dialog5";
			this.mDialogs = this.mDialogs || {};
			var oDialog = this.mDialogs[sDialogName];
			var oSource = oEvent.getSource();
			var oBindingContext = oSource.getBindingContext();
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oView;
			if (!oDialog) {
				this.getOwnerComponent().runAsOwner(function() {
					oView = sap.ui.xmlview({
						viewName: "com.sap.build.standard.twence1.view." + sDialogName
					});
					this.getView().addDependent(oView);
					oView.getController().setRouter(this.oRouter);
					oDialog = oView.getContent()[0];
					this.mDialogs[sDialogName] = oDialog;
				}.bind(this));
			}

			return new Promise(function(fnResolve) {
				oDialog.attachEventOnce("afterOpen", null, fnResolve);
				oDialog.open();
				if (oView) {
					oDialog.attachAfterOpen(function() {
						//	oDialog.rerender();
					});
				} else {
					oView = oDialog.getParent();
				}
				//				var oModel = this.getView().getModel();
				var oModel = sap.ui.getCore().getModel("jsonModel2");
				if (oModel) {
					oView.setModel(oModel);
				}
				sPath = "jsonModel2>/locaties";
				if (sPath) {
					var oParams = oView.getController().getBindingParameters();
					oView.bindObject({
						path: sPath,
						parameters: oParams
					});
				}
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});

		},
		onDialogPress: function() {
			if (!this.pressDialog) {
				this.pressDialog = new Dialog({
					title: 'Belangrijke Telefoonnummers',
					content: [
						new sap.m.Text({
							text: "\\n\Bij storing       : Helpdesk ICT            -   074 2404 455\\n\\n\Emmacase      : Binnendienst Nood     -  074 2404 567\\n\\n\CCR               : Teamleider                -  074 2404 555\\n\\n\Weegbrug      : Binnenkomend            - 074 2404 470\\n\\n\Weegbrug      : Administratie              -  074 2404 477\\n\\n\M & S            : Paul Burgers               -  074 2404 593\\n\\n\Wilfried Oude Reimer  -  074 2404 568"
						})
					],
					beginButton: new Button({
						text: 'Close',
						press: function() {
							this.pressDialog.close();
						}.bind(this)
					})
				});
				this.getView().addDependent(this.pressDialog);
			}
			this.pressDialog.open();
		},
		_onInputValueHelpRequest3: function(oEvent) {

			var sDialogName = "Dialog6";
			this.mDialogs = this.mDialogs || {};
			var oDialog = this.mDialogs[sDialogName];
			var oSource = oEvent.getSource();
			var oBindingContext = oSource.getBindingContext();
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oView;
			if (!oDialog) {
				this.getOwnerComponent().runAsOwner(function() {
					oView = sap.ui.xmlview({
						viewName: "com.sap.build.standard.twence1.view." + sDialogName
					});
					this.getView().addDependent(oView);
					oView.getController().setRouter(this.oRouter);
					oDialog = oView.getContent()[0];
					this.mDialogs[sDialogName] = oDialog;
				}.bind(this));
			}

			return new Promise(function(fnResolve) {
				oDialog.attachEventOnce("afterOpen", null, fnResolve);
				oDialog.open();
				if (oView) {
					oDialog.attachAfterOpen(function() {
						//	oDialog.rerender();
					});
				} else {
					oView = oDialog.getParent();
				}
				var oModel = sap.ui.getCore().getModel("jsonModel3");
				if (oModel) {
					oView.setModel(oModel);
				}
				//				sPath = "jsonModel3>/artikelen";
				if (sPath) {
					var oParams = oView.getController().getBindingParameters();
					oView.bindObject({
						path: sPath,
						parameters: oParams
					});
				}
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});

		},
		_onPositief: function(oEvent) {
			var oView = this.getView();
			var oBindingContext = oEvent.getSource().getBindingContext();
			var bindingContext = this.getView().getBindingContext();
			var property = bindingContext.getProperty("CasenrInsp");
			var Wdplantnr = bindingContext.getProperty("Wdplantnr");
			var ObjectGrp = bindingContext.getProperty("ObjectGrp");
			var Objnr = bindingContext.getProperty("Objnr");
			var FleetCat = bindingContext.getProperty("FleetCat");
			var Voorach = bindingContext.getProperty("Voorach");
			var Casetxt = bindingContext.getProperty("Casetxt");
			var sSoort = bindingContext.getProperty("Soort");
			//var sCharg = bindingContext.getProperty("Charg");
			var fcontainer = oView.byId("container").getValue();
			//	container
			var Omodelemmacase = oBindingContext.oModel.mContexts;
			//	var fPath = (oBindingContext) ? oBindingContext.getPath() : null;
			//		var sCharg = fPath = oBindingContext.getProperty("Charg");
			oView = this.getView();
			var sCharg = oView.byId("socharge").getText();
			//			var sControlId = "Kentekenset";
			//			var oControl = this.getView().byId(sControlId);
			var sQuery = oView.byId("kenteken").getValue();
			this.oKentekenTest = sQuery;
			var sSourceId = sap.ui.getCore().getModel("jsonModel1");
			var array = sSourceId.oData.kentekens.d.results;
			var kentekenarray = array.filter(function(kenteken) {
				return kenteken.Kenteken === sQuery;
			});
			//			console.log(kentekenarray);
			if (sSoort === "T" && kentekenarray.length <= 0) {
				sap.m.MessageToast.show("Kenteken is niet correct");
			} else if (sSoort === "T" && sCharg === "TEMP" && property === "") {
				MessageBox.error("Charge staat nog op Temp, Graag dit aanpassen");
				return;
			} else if (sSoort === "T" && fcontainer === "") {
				MessageBox.error("Het Container veld is nog leeg");
				return;
			} else {

				bindingContext = this.getView().getBindingContext();
				bindingContext.getModel().setProperty("VoertuigenSet/Signi", oView.byId("kenteken").getValue());
				//				bindingContext.setProperty("Signi", oView.byId("kenteken").getValue() );
				property = bindingContext.getProperty("CasenrInsp");
				if (property !== "") {
					var sDialogName = "Dialog31";
				}
				sDialogName = "Dialog31";
				var oModelEmma = new sap.ui.model.json.JSONModel("EmmaCase");
				oModelEmma.setData({
					Exti1: bindingContext.getProperty("CasenrInsp"),
					Signi: oView.byId("kenteken").getValue(),
					Waste: oView.byId("waste").getValue(),
					Lgpla: oView.byId("locatie").getValue(),
					ContNumber: oView.byId("container").getValue(),
					CasenrInsp: bindingContext.getProperty("CasenrInsp"),
					CtypeInsp: bindingContext.getProperty("CtypeInsp"),
					CasenrSamp: bindingContext.getProperty("CasenrSamp"),
					Ticketnr: bindingContext.getProperty("Ticketnr"),
					Doctype: bindingContext.getProperty("Doctype"),
					Casetxt: bindingContext.getProperty("Casetxt"),
					Legaldocument: bindingContext.getProperty("Legaldocument"),
					Transnr: bindingContext.getProperty("Transnr"),
					GvEmma: "ZMIP"
				});
				sap.ui.getCore().setModel(oModelEmma, "EmmaCase");

				this.mDialogs = this.mDialogs || {};
				var oDialog = this.mDialogs[sDialogName];
				var oSource = oEvent.getSource();
				oBindingContext = oSource.getBindingContext();
				//				var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
				//				oView;
				if (!oDialog) {
					this.getOwnerComponent().runAsOwner(function() {
						oView = sap.ui.xmlview({
							viewName: "com.sap.build.standard.twence1.view." + sDialogName
						});
						this.getView().addDependent(oView);
						oView.getController().setRouter(this.oRouter);
						oDialog = oView.getContent()[0];
						this.mDialogs[sDialogName] = oDialog;
					}.bind(this));
				}
			}
			if (typeof oDialog !== "undefined") {
				return new Promise(function(fnResolve) {
					oDialog.attachEventOnce("afterOpen", null, fnResolve);
					oDialog.open();

					if (oView) {
						oDialog.attachAfterOpen(function() {
							oDialog.rerender();
						});
					} else {
						oView = oDialog.getParent();
					}

					// var oModel = this.getView().getModel();
					// if (oModel) {
					// 	oView.setModel(oModel);
					// }
					// if (sPath) {
					// 	var oParams = oView.getController().getBindingParameters();
					// 	oView.bindObject({
					// 		path: sPath,
					// 		parameters: oParams
					// 	});
					// }
				}.bind(this)).catch(function(err) {
					if (err !== undefined) {
						MessageBox.error(err.message);
					}
				});
			}

		},
		_onNegatief: function(oEvent) {
			var oView = this.getView();
			var oBindingContext = oEvent.getSource().getBindingContext();
			var bindingContext = this.getView().getBindingContext();
			var property = bindingContext.getProperty("CasenrInsp");
			var sSoort = bindingContext.getProperty("Soort");
			var fcontainer = oView.byId("container").getValue();
			var sCharg = oView.byId("socharge").getText();
			var sQuery = oView.byId("kenteken").getValue();
			this.oKentekenTest = sQuery;
			var sSourceId = sap.ui.getCore().getModel("jsonModel1");
			var array = sSourceId.oData.kentekens.d.results;
			var kentekenarray = array.filter(function(kenteken) {
				return kenteken.Kenteken === sQuery;
			});
			if (sSoort === "T" && kentekenarray.length <= 0) {
				sap.m.MessageToast.show("Kenteken is niet correct");
			} else if (sSoort === "T" && sCharg === "TEMP" && property === "") {
				MessageBox.error("Charge staat nog op Temp, Graag dit aanpassen");
				return;
			} else if (sSoort === "T" && fcontainer === "") {
				MessageBox.error("De Container veld is nog leeg");
				return;
			} else {
				bindingContext.getModel().setProperty("VoertuigenSet/Signi", oView.byId("kenteken").getValue());
				bindingContext = this.getView().getBindingContext();
				property = bindingContext.getProperty("CasenrInsp");
				if (property !== "") {
					var sDialogName = "Dialog30";
				}
				sDialogName = "Dialog30";
				oView = this.getView();
				var oModelEmma = new sap.ui.model.json.JSONModel("EmmaCase");
				oModelEmma.setData({
					Exti1: bindingContext.getProperty("CasenrInsp"),
					Signi: oView.byId("kenteken").getValue(),
					Waste: oView.byId("waste").getValue(),
					Lgpla: oView.byId("locatie").getValue(),
					ContNumber: oView.byId("container").getValue(),
					CasenrInsp: bindingContext.getProperty("CasenrInsp"),
					CtypeInsp: bindingContext.getProperty("CtypeInsp"),
					CasenrSamp: bindingContext.getProperty("CasenrSamp"),
					Ticketnr: bindingContext.getProperty("Ticketnr"),
					Doctype: bindingContext.getProperty("Doctype"),
					Casetxt: bindingContext.getProperty("Casetxt"),
					Legaldocument: bindingContext.getProperty("Legaldocument"),
					Transnr: bindingContext.getProperty("Transnr"),
					GvEmma: "ZMIN"
				});
				sap.ui.getCore().setModel(oModelEmma, "EmmaCase");
				this.mDialogs = this.mDialogs || {};
				var oDialog = this.mDialogs[sDialogName];
				var oSource = oEvent.getSource();
				oBindingContext = oSource.getBindingContext();
				var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
				if (!oDialog) {
					this.getOwnerComponent().runAsOwner(function() {
						oView = sap.ui.xmlview({
							viewName: "com.sap.build.standard.twence1.view." + sDialogName
						});
						this.getView().addDependent(oView);
						oView.getController().setRouter(this.oRouter);
						oDialog = oView.getContent()[0];
						this.mDialogs[sDialogName] = oDialog;
					}.bind(this));
				}
			}
			if (typeof oDialog !== "undefined") {
				return new Promise(function(fnResolve) {
					oDialog.attachEventOnce("afterOpen", null, fnResolve);
					oDialog.open();
					if (oView) {
						oDialog.attachAfterOpen(function() {
							//	oDialog.rerender();
						});
					} else {
						oView = oDialog.getParent();
					}

					// var oModel = this.getView().getModel();
					// if (oModel) {
					// 	oView.setModel(oModel);
					// }
					// if (sPath) {
					// 	var oParams = oView.getController().getBindingParameters();
					// 	oView.bindObject({
					// 		path: sPath,
					// 		parameters: oParams
					// 	});
					// }
				}.bind(this)).catch(function(err) {
					if (err !== undefined) {
						MessageBox.error(err.message);
					}
				});
			}

		},
		_onAfwijking: function(oEvent) {
			var oView = this.getView();
			var oBindingContext = oEvent.getSource().getBindingContext();
			var bindingContext = this.getView().getBindingContext();
			var property = bindingContext.getProperty("CasenrInsp");
			var sSoort = bindingContext.getProperty("Soort");
			var fcontainer = oView.byId("container").getValue();
			oView = this.getView();
			var sCharg = oView.byId("socharge").getText();
			var sQuery = oView.byId("kenteken").getValue();
			this.oKentekenTest = sQuery;
			var sSourceId = sap.ui.getCore().getModel("jsonModel1");
			var array = sSourceId.oData.kentekens.d.results;
			var kentekenarray = array.filter(function(kenteken) {
				return kenteken.Kenteken === sQuery;
			});
			if (sSoort === "T" && kentekenarray.length <= 0) {
				sap.m.MessageToast.show("Kenteken is niet correct");
			} else if (sSoort === "T" && sCharg === "TEMP" && property === "") {
				MessageBox.error("Charge staat nog op Temp, Graag dit aanpassen");
				return;
			} else if (sSoort === "T" && fcontainer === "") {
				MessageBox.error("De Container veld is nog leeg");
				return;
			} else {

				if (property !== "") {
					var sDialogName = "Dialog30";
				}
				sDialogName = "Dialog30";

				var oModelEmma = new sap.ui.model.json.JSONModel("EmmaCase");
				oModelEmma.setData({
					Exti1: bindingContext.getProperty("CasenrInsp"),
					Signi: oView.byId("kenteken").getValue(),
					Waste: oView.byId("waste").getValue(),
					Lgpla: oView.byId("locatie").getValue(),
					ContNumber: oView.byId("container").getValue(),
					CasenrInsp: bindingContext.getProperty("CasenrInsp"),
					CtypeInsp: bindingContext.getProperty("CtypeInsp"),
					CasenrSamp: bindingContext.getProperty("CasenrSamp"),
					Ticketnr: bindingContext.getProperty("Ticketnr"),
					Doctype: bindingContext.getProperty("Doctype"),
					Casetxt: bindingContext.getProperty("Casetxt"),
					Legaldocument: bindingContext.getProperty("Legaldocument"),
					Transnr: bindingContext.getProperty("Transnr"),
					GvEmma: "ZSAM"
				});
				sap.ui.getCore().setModel(oModelEmma, "EmmaCase");

				this.mDialogs = this.mDialogs || {};
				var oDialog = this.mDialogs[sDialogName];
				var oSource = oEvent.getSource();
				oBindingContext = oSource.getBindingContext();
				var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
				if (!oDialog) {
					this.getOwnerComponent().runAsOwner(function() {
						oView = sap.ui.xmlview({
							viewName: "com.sap.build.standard.twence1.view." + sDialogName
						});
						this.getView().addDependent(oView);
						oView.getController().setRouter(this.oRouter);
						oDialog = oView.getContent()[0];
						this.mDialogs[sDialogName] = oDialog;
					}.bind(this));
				}
			}
			if (typeof oDialog !== "undefined") {
				return new Promise(function(fnResolve) {
					oDialog.attachEventOnce("afterOpen", null, fnResolve);
					oDialog.open();
					if (oView) {
						oDialog.attachAfterOpen(function() {
							//	oDialog.rerender();
						});
					} else {
						oView = oDialog.getParent();
					}
					// var oModel = this.getView().getModel();
					// if (oModel) {
					// 	oView.setModel(oModel);
					// }
					// if (sPath) {
					// 	var oParams = oView.getController().getBindingParameters();
					// 	oView.bindObject({
					// 		path: sPath,
					// 		parameters: oParams
					// 	});
					// }
				}.bind(this)).catch(function(err) {
					if (err !== undefined) {
						MessageBox.error(err.message);
					}
				});
			}

		},
		_onAccept: function(oEvent) {
			var oView = this.getView();
			var bindingContext = oEvent.getSource().getBindingContext();
			var property = bindingContext.getProperty("CasenrInsp");
			var fcontainer = oView.byId("container").getValue();
			var sSoort = bindingContext.getProperty("Soort");
			//	container
			// var Omodelemmacase = oBindingContext.oModel.mContexts;
			//	var fPath = (oBindingContext) ? oBindingContext.getPath() : null;
			//		var sCharg = fPath = oBindingContext.getProperty("Charg");
			oView = this.getView();
			var sCharg = oView.byId("socharge").getText();
			//			var sControlId = "Kentekenset";
			//			var oControl = this.getView().byId(sControlId);
			var sQuery = oView.byId("kenteken").getValue();
			this.oKentekenTest = sQuery;
			var sSourceId = sap.ui.getCore().getModel("jsonModel1");
			var array = sSourceId.oData.kentekens.d.results;
			var kentekenarray = array.filter(function(kenteken) {
				return kenteken.Kenteken === sQuery;
			});
			//			console.log(kentekenarray);
			if (sSoort === "T" && kentekenarray.length <= 0) {
				sap.m.MessageToast.show("Kenteken is niet correct");
			} else if (sSoort === "T" && sCharg === "TEMP" && property === "") {
				MessageBox.error("Charge staat nog op Temp, Graag dit aanpassen");
				return;
			} else if (sSoort === "T" && fcontainer === "") {
				MessageBox.error("Het Container veld is nog leeg");
				return;
			} else {
				var oBindingContext = oEvent.getSource().getBindingContext();
				var oView = this.getView();
				var oEntry = {};
				oEntry.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
				oEntry.Objnr = oBindingContext.getProperty("Objnr");
				oEntry.ObjectGrp = oBindingContext.getProperty("ObjectGrp");
				oEntry.FleetCat = oBindingContext.getProperty("FleetCat");
				oEntry.Voorach = oBindingContext.getProperty("Voorach");
				oEntry.Origin = oBindingContext.getProperty("Origin");
				oEntry.IdSap = oBindingContext.getProperty("IdSap");
				oEntry.Waste = oBindingContext.getProperty("Waste");
				oEntry.TransnrI = oBindingContext.getProperty("TransnrI");
				oEntry.WasteDesc = oBindingContext.getProperty("WasteDesc");
				oEntry.Charg = oBindingContext.getProperty("Charg");
				oEntry.Verif = oBindingContext.getProperty("Verif");
				oEntry.WdfDescription = oBindingContext.getProperty("WdfDescription");
				oEntry.LicenseNum = oBindingContext.getProperty("LicenseNum");
				oEntry.ContNumber = oBindingContext.getProperty("ContNumber");
				oEntry.Lbert = oView.byId("locatietekst").getText();
				oEntry.Lgpla = oBindingContext.getProperty("Lgpla");
				oEntry.ContractPosnr = oBindingContext.getProperty("ContractPosnr");
				oEntry.Approved = "X";
				oEntry.Reason = oBindingContext.getProperty("InspectieText");

				var oEntry2 = {};
				oEntry2.IdSap = oBindingContext.getProperty("IdSap");
				oEntry2.ContNumber = oBindingContext.getProperty("ContNumber");
				oEntry2.Soort = oBindingContext.getProperty("Soort");
				oEntry2.Contract = oBindingContext.getProperty("Contract");
				oEntry2.Transporter = oBindingContext.getProperty("Transporter");
				oEntry2.Waste = oBindingContext.getProperty("Waste");
				oEntry2.Lgtyp = oBindingContext.getProperty("Lgtyp");
				oEntry2.Voorach = oBindingContext.getProperty("Voorach");
				oEntry2.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
				oEntry2.Objnr = oBindingContext.getProperty("Objnr");
				oEntry2.ObjectGrp = oBindingContext.getProperty("ObjectGrp");
				oEntry2.FleetCat = oBindingContext.getProperty("FleetCat");
				oEntry2.LicenseNum = oBindingContext.getProperty("LicenseNum");

				// oDetails.Origin = oView.byId("origin").getText();
				// oDetails.IdSap = oView.byId("idsap").getText();
				// oDetails.Voorach = oView.byId("voorach").getText();
				// oDetails.Lgpla = oView.byId("locatie").getValue();
				// oDetails.Waste = oView.byId("waste").getValue();
				// oDetails.ContNumber = oView.byId("container").getValue();
				// oDetails.WasteDesc = oView.byId("wastedesc").getText();
				// oDetails.Lbert = oView.byId("locatietekst").getText();
				// oDetails.Name1 = oView.byId("transporteur").getText();

				var sRoot = "ChecksSet(Wdplantnr='" + oEntry2.Wdplantnr + "',Objnr='" + oEntry2.Objnr + "',ObjectGrp='" + oEntry2.ObjectGrp +
					"',FleetCat='" + oEntry2.FleetCat + "',Voorach='" + oEntry2.Voorach + "',Soort='" + oEntry2.Soort + "',Contract='" + oEntry2.Contract +
					"',Waste='" + oView.byId("waste").getValue() + "',Transporter='" + oEntry2.Transporter + "',Lgtyp='" + oEntry2.Lgtyp +
					"',ContNumber='" +
					oView.byId("container").getValue() + "',LicenseNum='" + oView.byId("kenteken").getValue() + "',Status='',IdSap='" + oEntry2.IdSap +
					"')";

				while (sRoot.search("/") !== -1) {
					sRoot = sRoot.replace("/", "%2F");
				}
				var sURL = "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/" + sRoot;

				var oModelSaveChecks = new sap.ui.model.json.JSONModel();
				//			var dataSaveChecks;

				$.ajax({
					type: "GET",
					url: sURL,
					async: false,
					dataType: "json",
					success: function(dataSaveChecks, textStatus, jqXHR) {
						sap.ui.getCore().setModel(oModelSaveChecks, "jsonModelSaveChecks");
						oModelSaveChecks.setData({
							savechecks: dataSaveChecks
						});
					},
					error: function(jqXHR, textStatus, errorThrown) {
						sap.m.MessageToast.show("NOT OK");
					}

				});

				var oModelCheck = sap.ui.getCore().getModel("jsonModelSaveChecks");

				var sStatus = oModelCheck.oData.savechecks.d.Status;

				if (sStatus !== "") {
					sap.m.MessageToast.show(sStatus);
				} else {

					var oDetails = {};

					//				oDetails.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
					oDetails.Origin = oView.byId("origin").getText();
					oDetails.IdSap = oView.byId("idsap").getText();
					oDetails.Voorach = oView.byId("voorach").getText();
					oDetails.Signi = oView.byId("kenteken").getValue();
					oDetails.Lgpla = oView.byId("locatie").getValue();
					oDetails.Waste = oView.byId("waste").getValue();
					oDetails.ContNumber = oView.byId("container").getValue();
					oDetails.WasteDesc = oView.byId("wastedesc").getText();
					oDetails.Lbert = oView.byId("locatietekst").getText();
					oDetails.Name1 = oView.byId("transporteur").getText();
					oDetails.Milieuclass = oView.byId("mtype").getUnit();
					oDetails.Charg = oView.byId("socharge").getText();
					var oModel = this.getOwnerComponent().getModel();
					oModel.setUseBatch(false);
					sURL = "/UpdateDetailsSet(Origin='" + oDetails.Origin + "',IdSap='" + oDetails.IdSap + "')";
					var that = this;

					oModel.update(sURL, oDetails, {
						method: "PUT",
						success: function(data) {
							sURL = "/SaveEndSet(Origin='" + oDetails.Origin + "',IdSap='" + oDetails.IdSap + "',Voorach='" + oDetails.Voorach + "')";
							var oModelOdata = that.getOwnerComponent().getModel();
							// oModelOdata.setUseBatch(true);
							oModelOdata.update(sURL, oEntry, {
								//					method: "PUT",
								success: function() {
									return new Promise(function(fnResolve) {
										that.doNavigate("Transport", oBindingContext, fnResolve, "");
										window.location.reload();
									}.bind(that)).catch(function(err) {
										if (err !== undefined) {
											//										sap.ui.commons.MessageBox.alert("Error!");
										}
									});
								},
								error: function() {
									sap.ui.commons.MessageBox.alert("Error!");
								}
							});
						},
						error: function(e) {
							sap.ui.commons.MessageBox.alert("Error!");
						}
					});

				}
			}
		},
		_onReject: function(oEvent) {
			var oView = this.getView();
			var bindingContext = oEvent.getSource().getBindingContext();
			var property = bindingContext.getProperty("CasenrInsp");
			var fcontainer = oView.byId("container").getValue();
			var sSoort = bindingContext.getProperty("Soort");
			//	container

			//	var fPath = (oBindingContext) ? oBindingContext.getPath() : null;
			//		var sCharg = fPath = oBindingContext.getProperty("Charg");
			oView = this.getView();
			var sCharg = oView.byId("socharge").getText();
			//			var sControlId = "Kentekenset";
			//			var oControl = this.getView().byId(sControlId);
			var sQuery = oView.byId("kenteken").getValue();
			this.oKentekenTest = sQuery;
			var sSourceId = sap.ui.getCore().getModel("jsonModel1");
			var array = sSourceId.oData.kentekens.d.results;
			var kentekenarray = array.filter(function(kenteken) {
				return kenteken.Kenteken === sQuery;
			});
			//			console.log(kentekenarray);
			if (sSoort === "T" && kentekenarray.length <= 0) {
				sap.m.MessageToast.show("Kenteken is niet correct");
			} else if (sSoort === "T" && sCharg === "TEMP" && property === "") {
				MessageBox.error("Charge staat nog op Temp, Graag dit aanpassen");
				return;
			} else if (sSoort === "T" && fcontainer === "") {
				MessageBox.error("Het Container veld is nog leeg");
				return;
			} else {

				var oBindingContext = oEvent.getSource().getBindingContext();

				var oEntry = {};

				oEntry.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
				oEntry.Objnr = oBindingContext.getProperty("Objnr");
				oEntry.ObjectGrp = oBindingContext.getProperty("ObjectGrp");
				oEntry.FleetCat = oBindingContext.getProperty("FleetCat");
				oEntry.Voorach = oBindingContext.getProperty("Voorach");
				oEntry.Origin = oBindingContext.getProperty("Origin");
				oEntry.IdSap = oBindingContext.getProperty("IdSap");
				oEntry.Waste = oBindingContext.getProperty("Waste");
				oEntry.TransnrI = oBindingContext.getProperty("TransnrI");
				oEntry.WasteDesc = oBindingContext.getProperty("WasteDesc");
				oEntry.Charg = oBindingContext.getProperty("Charg");
				oEntry.Verif = oBindingContext.getProperty("Verif");
				oEntry.WdfDescription = oBindingContext.getProperty("WdfDescription");
				oEntry.LicenseNum = oBindingContext.getProperty("LicenseNum");
				oEntry.ContNumber = oBindingContext.getProperty("ContNumber");
				oEntry.Lbert = oView.byId("locatietekst").getText();
				oEntry.Lgpla = oBindingContext.getProperty("Lgpla");
				oEntry.ContractPosnr = oBindingContext.getProperty("ContractPosnr");
				oEntry.Approved = "";
				oEntry.Reason = oBindingContext.getProperty("InspectieText");

				var oEntry2 = {};
				oEntry2.ContNumber = oBindingContext.getProperty("ContNumber");
				oEntry2.Soort = oBindingContext.getProperty("Soort");
				oEntry2.Contract = oBindingContext.getProperty("Contract");
				oEntry2.Transporter = oBindingContext.getProperty("Transporter");
				oEntry2.Waste = oBindingContext.getProperty("Waste");
				oEntry2.Lgtyp = oBindingContext.getProperty("Lgtyp");
				oEntry2.Voorach = oBindingContext.getProperty("Voorach");
				oEntry2.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
				oEntry2.Objnr = oBindingContext.getProperty("Objnr");
				oEntry2.ObjectGrp = oBindingContext.getProperty("ObjectGrp");
				oEntry2.FleetCat = oBindingContext.getProperty("FleetCat");
				oEntry2.LicenseNum = oBindingContext.getProperty("LicenseNum");

				var sRoot = "ChecksSet(Wdplantnr='" + oEntry2.Wdplantnr + "',Objnr='" + oEntry2.Objnr + "',ObjectGrp='" + oEntry2.ObjectGrp +
					"',FleetCat='" + oEntry2.FleetCat + "',Voorach='" + oEntry2.Voorach + "',Soort='" + oEntry2.Soort + "',Contract='" + oEntry2.Contract +
					"',Waste='" + oEntry2.Waste + "',Transporter='" + oEntry2.Transporter + "',Lgtyp='" + oEntry2.Lgtyp + "',ContNumber='" +
					oBindingContext.getProperty("ContNumber") + "',LicenseNum='" + oEntry2.LicenseNum + "',Status='',IdSap='" + oEntry2.IdSap + "')";

				while (sRoot.search("/") !== -1) {
					sRoot = sRoot.replace("/", "%2F");
				}
				var sURL = "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/" + sRoot;

				var oModelSaveChecks = new sap.ui.model.json.JSONModel();
				//			var dataSaveChecks;

				$.ajax({
					type: "GET",
					url: sURL,
					async: false,
					dataType: "json",
					success: function(dataSaveChecks, textStatus, jqXHR) {
						sap.ui.getCore().setModel(oModelSaveChecks, "jsonModelSaveChecks");
						oModelSaveChecks.setData({
							savechecks: dataSaveChecks
						});
					},
					error: function(jqXHR, textStatus, errorThrown) {
						sap.m.MessageToast.show("NOT OK");
					}
				});

				var oModelCheck = sap.ui.getCore().getModel("jsonModelSaveChecks");

				var sStatus = oModelCheck.oData.savechecks.d.Status;

				if (sStatus !== "") {
					sap.m.MessageToast.show(sStatus);
				} else {

					var oDetails = {};
					//				oDetails.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
					oDetails.Origin = oView.byId("origin").getText();
					oDetails.IdSap = oView.byId("idsap").getText();
					oDetails.Voorach = oView.byId("voorach").getText();
					oDetails.Signi = oView.byId("kenteken").getValue();
					oDetails.Lgpla = oView.byId("locatie").getValue();
					oDetails.Waste = oView.byId("waste").getValue();
					oDetails.ContNumber = oView.byId("container").getValue();
					oDetails.WasteDesc = oView.byId("wastedesc").getText();
					oDetails.Lbert = oView.byId("locatie").getValue();
					oDetails.Name1 = oView.byId("transporteur").getText();
					oDetails.Milieuclass = oView.byId("mtype").getUnit();
					oDetails.Charg = oView.byId("socharge").getText();
					var oModel = this.getOwnerComponent().getModel();
					oModel.setUseBatch(false);
					sURL = "/UpdateDetailsSet(Origin='" + oDetails.Origin + "',IdSap='" + oDetails.IdSap + "')";
					var that = this;
					oModel.update(sURL, oDetails, {
						method: "PUT",
						success: function(data) {
							sURL = "/SaveEndSet(Origin='" + oDetails.Origin + "',IdSap='" + oDetails.IdSap + "',Voorach='" + oDetails.Voorach + "')";
							var oModelOdata = that.getOwnerComponent().getModel();
							// oModelOdata.setUseBatch(true);
							oModelOdata.update(sURL, oEntry, {
								//					method: "PUT",
								success: function() {
									return new Promise(function(fnResolve) {
										that.doNavigate("Transport", oBindingContext, fnResolve, "");
										window.location.reload();
									}.bind(that)).catch(function(err) {
										if (err !== undefined) {
											//										sap.ui.commons.MessageBox.alert("Error!");
										}
									});
								},
								error: function() {
									sap.ui.commons.MessageBox.alert("Error!");
								}
							});
						},
						error: function(e) {
							sap.ui.commons.MessageBox.alert("Error!");
						}
					});

				}
			}
		},

		onInit: function(oEvent) {

			this.mBindingOptions = {};
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("Details").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
			this._lockThisTransport;
		}
	});
}, /* bExport= */ true);