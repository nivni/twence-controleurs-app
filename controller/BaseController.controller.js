sap.ui.define([
	'jquery.sap.global',
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/model/json/JSONModel"
], function(jQuery, Controller, History, JSONModel) {
	"use strict";
	//
	return Controller.extend("com.sap.build.standard.twence1.BaseController", {
		oEntry: {},
		oCategory: {},
		oAnswers: {}

	});

});