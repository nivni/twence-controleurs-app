sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./utilities",
	"sap/ui/core/routing/History",	"sap/m/Dialog",
	"sap/m/Button"
], function(BaseController, MessageBox, Utilities, History, Dialog, Button) {
	"use strict";

	return BaseController.extend("com.sap.build.standard.twence1.controller.Wdf", {
		handleRouteMatched: function(oEvent) {

			var oParams = {};


			if (oEvent.mParameters.data.context) {
				this.sContext = oEvent.mParameters.data.context;
				var oPath;
				if (this.sContext) {
					oPath = {
						path: "/" + this.sContext,
						parameters: oParams
					};
					this.getView().bindObject(oPath);

					var oItem = this.getView();
					var model = oItem.getModel();
					var dataContext = model.getProperty("/" + this.sContext);
					
						var dataEmma;

					sap.ui.getCore().setModel(dataEmma, "jsonModelEmma");
					dataEmma.setData({
						fotolijst: dataEmma
					});

					var sRoot = "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/WDPlantSet";
					//var sUrl = sRoot.concat(dataContext['CasenrInsp'], "'");

					$.ajax({
						type: "GET",
						url: sRoot,
						dataType: "json",
						success: function(dataEmma, textStatus, jqXHR) {
							//console.log(Data);
							sap.ui.getCore().setModel(dataEmma, "jsonModelEmma");
							dataEmma.setData({
								fotolijst: dataEmma
							});

							//	sap.m.MessageToast.show("OK");
							//After you get the data you can set the data to any field as required.
						},
						error: function(jqXHR, textStatus, errorThrown) {
							sap.m.MessageToast.show("NOT OK");
						}
					});
					var getusername = dataEmma;
				}
			}
		},
	
		
		wait: function(ms) {
			var start = new Date().getTime();
			var end = start;
			while (end < start + ms) {
				end = new Date().getTime();
			}
		},
			onRefreshPress: function() {
			window.location.reload();
		},
		_onButtonPress3: function(oEvent) {
			var oBindingContext = oEvent.getSource().getBindingContext();
			var oModel = this.getView().getModel();
			oModel.refresh(true);
			return new Promise(function(fnResolve) {
				this.doNavigate("Transport", oBindingContext, fnResolve, "");
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});
		},
				onDialogPress: function () {
			if (!this.pressDialog) {
				this.pressDialog = new Dialog({
					title: 'Belangrijke Telefoonnummers',
					content:  [
									new sap.m.Text({
										text : "\\n\Bij storing       : Helpdesk ICT            -   074 2404 455\\n\\n\Emmacase      : Binnendienst Nood     -  074 2404 567\\n\\n\CCR               : Teamleider                -  074 2404 555\\n\\n\Weegbrug      : Binnenkomend            - 074 2404 470\\n\\n\Weegbrug      : Administratie              -  074 2404 477\\n\\n\M & S            : Paul Burgers               -  074 2404 593\\n\\n\Wilfried Oude Reimer  -  074 2404 568"
									})
								],
					beginButton: new Button({
						text: 'Close',
						press: function () {
							this.pressDialog.close();
						}.bind(this)
					})
				});
				this.getView().addDependent(this.pressDialog);
			}
			this.pressDialog.open();
		},

	_showuser: 	function() {
		var name = this.getView().byId("WDF_select").getItems()[0].getBindingContext().getProperty("Controleur");
		var welkom = "U bent ingelogt als gebruiker:";
		var UserText = welkom+name;
		var authtic = "Basic 1324654987984567897464654"+btoa+username;
		var close = "close";
		var username = "(USERNAME +  + PASSWORD,+ )";
				var oShell = new sap.m.Shell();
				var dialog = new Dialog({
				title: 'User',
				type: 'Message',
				
				content: new sap.m.Text({ text : UserText }),
				beginButton: new Button({
					text: 'logout',
					press: function () {
						oShell.destroyApp();
					    sap.ui.getCore().applyChanges();
					    	$.ajax({
								url: '/sap/opu/odata/sap/public/bc/icf/logoff',
								type: 'GET',
								headers: {
								Authorization: authtic + btoa(username),
								Connection: close, username: "COPS_USER", password: "init123"},
								 data: {},
								processData: true,
								success: function(data) {
									console.log("success" + data);
								},
								error: function(e) {
									console.log("error: " + e);
								}
							});
					    jQuery(document.body).html("<span>Logged out successfully.</span>");
					    window.open('','_self').close();
						dialog.close();
					}
				}),
				endButton: new Button({
					text: 'Ok',
					press: function () {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});

			dialog.open();
	/*	var text = this.getView().byId("username");
		text.setWrapping(!text.getWrapping());
		this.getView().byId("username").setText(text.getWrapping() ? name : name);*/
		//var sId = getView().byId("username");  
	//	sap.ui.getCore().byId(sId).setValue(oSelectedItem.getTitle());
	//	sap.ui.getCore().byId("username");
	//	sap.m.Text.setText(name);
		//this.getId(sId).setText(name);
		
	},

		doNavigate: function(sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {

			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = (oBindingContext) ? oBindingContext.getModel() : null;

			var sEntityNameSet;
			if (sPath !== null && sPath !== "") {
				if (sPath.substring(0, 1) === "/") {
					sPath = sPath.substring(1);
				}
				sEntityNameSet = sPath.split("(")[0];
			}
			var sNavigationPropertyName;
			var sMasterContext = this.sMasterContext ? this.sMasterContext : sPath;

			if (sEntityNameSet !== null) {
				sNavigationPropertyName = sViaRelation || this.getOwnerComponent().getNavigationPropertyForNavigationWithContext(sEntityNameSet,
					sRouteName);
			}
			if (sNavigationPropertyName !== null && sNavigationPropertyName !== undefined) {
				if (sNavigationPropertyName === "") {
					this.oRouter.navTo(sRouteName, {
						context: sPath,
						masterContext: sMasterContext
					}, false);
				} else {
					oModel.createBindingContext(sNavigationPropertyName, oBindingContext, null, function(bindingContext) {
						if (bindingContext) {
							sPath = bindingContext.getPath();
							if (sPath.substring(0, 1) === "/") {
								sPath = sPath.substring(1);
							}
						} else {
							sPath = "undefined";
						}

						// If the navigation is a 1-n, sPath would be "undefined" as this is not supported in Build
						if (sPath === "undefined") {
							this.oRouter.navTo(sRouteName);
						} else {
							this.oRouter.navTo(sRouteName, {
								context: sPath,
								masterContext: sMasterContext
							}, false);
						}
					}.bind(this));
				}
			} else {
				this.oRouter.navTo(sRouteName);
			}

			if (typeof fnPromiseResolve === "function") {
				fnPromiseResolve();
			}
		},
		onInit: function() {
			
			
			this.mBindingOptions = {};
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("Wdf").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
			
	
		},


		ShowUsername: function(oEvent, sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {
			var oUserName = $.session.getUsername();
		var	username = oEvent.getUser().getFullName();
			alert(username);

		},




		handleSelect: function(oEvent) {
//			sap.m.MessageToast.show("This message should appear in the message toast");
			var oModel = this.getView().getModel();
			var oEntry = {};
			
			var oSource = oEvent.getSource();
			oEntry.Wdplantnr = oSource.mProperties.text;
			oEntry.Mark = oSource.mProperties.selected;
			oEntry.Controleur = "";

			oModel.create("/WDFSelectSet", oEntry, {
				method: "POST",
				success: function(data) {
//					alert("success");
				},
				error: function(e) {
//					alert("error");
				}
			});

		}
	});
}, /* bExport= */ true);