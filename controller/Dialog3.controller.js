sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/m/UploadCollectionParameter",
	"./utilities",
	"sap/ui/core/routing/History"
], function(BaseController, MessageBox, MessageToast, UploadCollectionParameter, Utilities, History, JSONModel) {
	"use strict";

	return BaseController.extend("com.sap.build.standard.twence1.controller.Dialog3", {
		setRouter: function(oRouter) {
			this.oRouter = oRouter;
			var EMMACASE = ["0000000001"] ;// hardcoded for example
			var CasenrInsp = ["$filter=CasenrInsp eq '" + EMMACASE + "'"];
		},
			handleUploadComplete: function(oEvent) {
			var sResponse = oEvent.getParameter("response");
			if (sResponse) {
				var MessageBox = "";
				var m = /^\[(\d\d\d)\]:(.*)$/.exec(sResponse);
				if (m[1] == "200") {
					MessageBox = "Return Code: " + m[1] + "\n" + m[2] + "(Upload Success)";
					oEvent.getSource().setValue("");
				} else {
					MessageBox = "Return Code: " + m[1] + "\n" + m[2] + "(Upload Error)";
				}

				MessageToast.show(MessageBox);
			}
		},
		onFileDeleted: function(oEvent) {
			MessageToast.show("Event 1");
		},
		deleteItemById: function(sItemToDeleteId) {
					MessageToast.show("Event 2");

		},

		
		onStartUpload: function(oEvent) {
			var EMMACASE = ["0000000001"] ;// hardcoded for example
			var oUploadCollection = this.getView().byId("UploadCollection");
			var oTextArea = this.getView().byId("TextArea");
			var cFiles = oUploadCollection.getItems().length;
			var uploadInfo = cFiles + " file(s)";

			if (cFiles > 0) {
				oUploadCollection.upload();

				if (oTextArea.getValue().length === 0) {
					uploadInfo = uploadInfo + " without notes";
				} else {
					uploadInfo = uploadInfo + " with notes";
				}

				MessageToast.show("Method Upload is called (" + uploadInfo + ")");
				MessageBox.information("Uploaded " + uploadInfo);
				oTextArea.setValue("");
			}
		},
		
		handleRouteMatched: function(oEvent) {
					
			var oParams = {};

			if (oEvent.mParameters.data.context) {
				this.sContext = oEvent.mParameters.data.context;
				var oPath;
				if (this.sContext) {
					oPath = {
						path: "/" + this.sContext,
						parameters: oParams
					};
					this.getView().bindObject(oPath);
				}
			}

		},
		handleUploadPress: function(oEvent) {
			var oFileUploader = this.getView().byId("fileUploader");
			oFileUploader.upload();
		},
		getBindingParameters: function() {
			return {

};
		},
		_onButtonPress6: function() {
			var oDialog = this.getView().getContent()[0];

			return new Promise(function(fnResolve) {
				oDialog.attachEventOnce("afterClose", null, fnResolve);
				oDialog.close();
			});

		},
		_onButtonPress5: function() {
			var oDialog = this.getView().getContent()[0];

			return new Promise(function(fnResolve) {
				oDialog.attachEventOnce("afterClose", null, fnResolve);
				oDialog.close();
			});

		},
		onInit: function() {
			this.mBindingOptions = {};
			this._oDialog = this.getView().getContent()[0];
			
			/* for the tree */
			var oModel =  new sap.ui.model.json.JSONModel();
			oModel.loadData("../localService/Tree.json");
			this.getView().byId("Tree").setModel(oModel);

		},
		onExit: function() {
			this._oDialog.destroy();

		}
	});
}, /* bExport= */ true);