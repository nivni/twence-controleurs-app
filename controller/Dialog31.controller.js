sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/m/UploadCollectionParameter",
	"./utilities",
	"sap/ui/core/routing/History",
	'sap/m/Button',
	'sap/m/Dialog',
	'sap/m/Text',
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(BaseController, MessageBox, MessageToast, UploadCollectionParameter, Utilities, History, Button, Dialog, Text, JSONModel,
	Filter, FilterOperator) {
	"use strict";

	return BaseController.extend("com.sap.build.standard.twence1.controller.Dialog31", {

		oAfwijzingRedenen: "",

		setRouter: function(oRouter) {
			this.oRouter = oRouter;
		},
		handleUploadComplete: function(oEvent) {
			var sResponse = oEvent.getParameter("response");
			if (sResponse) {
				MessageBox = "";
				var m = /^\[(\d\d\d)\]:(.*)$/.exec(sResponse);
				if (m[1] === "200") {
					MessageBox = "Return Code: " + m[1] + "\n" + m[2] + "(Upload Success)";
					oEvent.getSource().setValue("");
				} else {
					MessageBox = "Return Code: " + m[1] + "\n" + m[2] + "(Upload Error)";
				}

				MessageToast.show(MessageBox);
			}
		},
		Popup_img: function(oEvent) {

			var oFileName = oEvent.oSource.mProperties.fileName;
			var begin = "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/FileSet('";
			var end = "')/$value";
			var urls = begin + oFileName + end;
			var oImage = new sap.m.Image({
				width: "100%"
			});
			oImage.setSrc(urls);
			//oImage.setAlt("alternative image text for image");
			var dialog = new Dialog({
				title: 'Preview',
				type: 'Message',
				content: oImage,
				beginButton: new Button({
					text: 'OK',
					press: function() {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});

			dialog.open();
		},
		onToggleOpenState: function(oEvent) {
			var iItemIndex = oEvent.getParameter("itemIndex");
			var oItemContext = oEvent.getParameter("itemContext");
			var bExpanded = oEvent.getParameter("expanded");

			MessageToast.show("Item index: " + iItemIndex + "\nItem context (path): " + oItemContext + "\nExpanded: " + bExpanded, {
				duration: 5000,
				width: "auto"
			});
		},

		loadData: function(oModel, sPath, iLevel) {
			var oTree = this.getView().byId("Tree");
			// In this example we are just pretending to load data from the backend.
			oTree.setBusy(true);
			setTimeout(function() {
				var aNewNodes = [{
					text: "Node" + new Array(iLevel == null ? 2 : iLevel + 3).join("-1")
				}, {
					text: "Node" + new Array(iLevel == null ? 2 : iLevel + 3).join("-2"),
					nodes: [{ // This dummy node is required to get an expandable item.
						text: iLevel === 5 ? "Last node" : "",
						dummy: !iLevel || iLevel < 5
					}]
				}];
				oModel.setProperty(sPath ? sPath + "/nodes" : "/", aNewNodes);
				oTree.setBusy(false);
			}, 2000);
		},
		doNavigate: function(sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {

			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = (oBindingContext) ? oBindingContext.getModel() : null;

			var sEntityNameSet;
			if (sPath !== null && sPath !== "") {
				if (sPath.substring(0, 1) === "/") {
					sPath = sPath.substring(1);
				}
				sEntityNameSet = sPath.split("(")[0];
			}
			var sNavigationPropertyName;
			var sMasterContext = this.sMasterContext ? this.sMasterContext : sPath;

			if (sEntityNameSet !== null) {
				sNavigationPropertyName = sViaRelation || this.getOwnerComponent().getNavigationPropertyForNavigationWithContext(sEntityNameSet,
					sRouteName);
			}
			if (sNavigationPropertyName !== null && sNavigationPropertyName !== undefined) {
				if (sNavigationPropertyName === "") {
					this.oRouter.navTo(sRouteName, {
						context: sPath,
						masterContext: sMasterContext
					}, false);
				} else {
					oModel.createBindingContext(sNavigationPropertyName, oBindingContext, null, function(bindingContext) {
						if (bindingContext) {
							sPath = bindingContext.getPath();
							if (sPath.substring(0, 1) === "/") {
								sPath = sPath.substring(1);
							}
						} else {
							sPath = "undefined";
						}

						// If the navigation is a 1-n, sPath would be "undefined" as this is not supported in Build
						if (sPath === "undefined") {
							this.oRouter.navTo(sRouteName);
						} else {
							this.oRouter.navTo(sRouteName, {
								context: sPath,
								masterContext: sMasterContext
							}, false);
						}
					}.bind(this));
				}
			} else {
				this.oRouter.navTo(sRouteName);
			}

			if (typeof fnPromiseResolve === "function") {
				fnPromiseResolve();
			}
		},
		onFileDeleted: function(oEvent) {
			this.deleteItemById(oEvent.getParameter("documentId"));
			MessageToast.show("Event fileDeleted triggered");
		},
		deleteItemById: function(sItemToDeleteId) {
			MessageToast.show("Event fileDeleted triggered");

		},

		onChange: function(oEvent) {
			var oUploadCollection = oEvent.getSource();
			// Header Token
			var csrfToken = this.getView().getModel().oHeaders['x-csrf-token'];
			//			oUploadCollection.setSendXHR(true);
			var oCustomerHeaderToken = new UploadCollectionParameter({
				name: "x-csrf-token",
				value: csrfToken
			});
			oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
		},

		onUploadComplete: function(oEvent) {
			setTimeout(function() {
				//  _busyDialog.close();
				MessageToast.show("Foto's geüpload");
			}, 3000);
		},

		onStartUpload: function(oEvent) {

			var oModelEmma = sap.ui.getCore().getModel("EmmaCase");

			var oUploadCollection = this.getView().byId("UploadCollection");
			var cFiles = oUploadCollection.getItems().length;
			var uploadInfo = cFiles + " file(s)";
			var resourcemodel = this.getOwnerComponent().getModel();
			var oDataResource = {
				sServiceUrl: resourcemodel.sServiceUrl + "/FileSet"
			};
			var jsonResource = new sap.ui.model.json.JSONModel(oDataResource);
			this.getView().setModel(jsonResource, "ResourceModel");
			if (cFiles > 0) {
				oUploadCollection = this.getView().byId("UploadCollection");

				var sServiceUrl = resourcemodel.sServiceUrl + "/FileSet";
				for (var i = 0; i < oUploadCollection._aFileUploadersForPendingUpload.length; i++) {

					var sCasenrInsp = oModelEmma.oData.CasenrInsp;
					var oCustomerHeaderSlug = new sap.ui.unified.FileUploaderParameter({
						name: "slug",
						value: sCasenrInsp + "_0" + i + ".jpg"
					});

					oUploadCollection.NewModel = window.location.origin + "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/FileSet";
					sServiceUrl = window.location.origin + "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/FileSet";
					oUploadCollection._aFileUploadersForPendingUpload[i].setUploadUrl(sServiceUrl);
					oUploadCollection._aFileUploadersForPendingUpload[i].addHeaderParameter(oCustomerHeaderSlug);
				}

				oUploadCollection.upload();

				MessageToast.show("Method Upload is called (" + uploadInfo + ")");
				MessageBox.information("Uploaded " + uploadInfo);
			}

		},

		handleRouteMatched: function(oEvent) {
			var oParams = {};
			if (oEvent.mParameters.data.context) {
				this.sContext = oEvent.mParameters.data.context;
				var oPath;
				if (this.sContext) {
					oPath = {
						path: "/" + this.sContext,
						parameters: oParams
					};
					this.getView().bindObject(oPath);
				}
			}
		},

		handleUploadPress: function(oEvent) {
			var oFileUploader = this.getView().byId("{Filename}");
			oFileUploader.upload();
		},
		getBindingParameters: function() {
			return {};
		},

		_onAccept: function(oEvent) {
			var pView = this.getView().getParent();
			var oUploadCollection = this.getView().byId("UploadCollection");
			var oTextEmmaCase = this.getView().byId("Casetxt");
			var cFiles = oUploadCollection.getItems().length;
			var Casetxt = oTextEmmaCase.mProperties.value;
			var kentTeken = pView.byId("kenteken").getValue();
			var fcontainer = pView.byId("container").getValue();
			var resourcemodel = this.getOwnerComponent().getModel();
			var oDataResource = {
				sServiceUrl: resourcemodel.sServiceUrl + "/FileSet"
			};
			if (cFiles > 4) {
				sap.m.MessageToast.show("Op dit moment heb je meer dat 4 fotos geselecteerd op upteloaden");
			} else if (cFiles < 1) {
				sap.m.MessageToast.show("Je moet minimaal 3 fotos uploaden om een emma case aan te maken");
			} else {
				var oModelEmma = sap.ui.getCore().getModel("EmmaCase");
				var oEntry = {};
				oEntry.CasenrInsp = oModelEmma.oData.CasenrInsp;
				oEntry.CtypeInsp = oModelEmma.oData.CtypeInsp;
				oEntry.CasenrSamp = oModelEmma.oData.CasenrSamp;
				oEntry.Ticketnr = oModelEmma.oData.Ticketnr;
				oEntry.Doctype = oModelEmma.oData.Doctype;
				oEntry.Casetxt = Casetxt;
				oEntry.Lgpla = fcontainer;
				oEntry.ContNumber = fcontainer;
				oEntry.Signi = kentTeken;
				oEntry.Legaldocument = oModelEmma.oData.Legaldocument;
				oEntry.Transnr = oModelEmma.oData.Transnr;
				oEntry.GvEmma = oModelEmma.oData.GvEmma;
				oEntry.Approved = 'X';
			}
			var oCasenrInsp = oModelEmma.oData.CasenrInsp;
			cFiles = oUploadCollection.getItems().length;
			if (oCasenrInsp === '') {
				var sRoot = "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/EmmaCaseSet(CasenrInsp='',CtypeInsp='" + oEntry.CtypeInsp + "',CasenrSamp='" +
					oEntry.CasenrSamp + "',Ticketnr='" + oEntry.Ticketnr + "',Doctype='" + oEntry.Doctype + "',Casetxt='" + oEntry.Casetxt +
					"',Legaldocument='" + oEntry.Legaldocument + "',Transnr='" + oEntry.Transnr + "',GvEmma='" + oEntry.GvEmma + "',ErrorText='')";
				var sUrl = sRoot;

				var oModelEmmaCase = new sap.ui.model.json.JSONModel();

				$.ajax({
					type: "GET",
					url: sUrl,
					async: false,
					dataType: "json",
					success: function(dataEmmaCase, textStatus, jqXHR) {
						sap.ui.getCore().setModel(oModelEmmaCase, "jsonModelEmmaCase");
						oModelEmmaCase.setData({
							emmacase: dataEmmaCase
						});
					},
					error: function(jqXHR, textStatus, errorThrown) {
						//	sap.m.MessageToast.show("NOT OK");
					}
				});
				var oBindingContext = oEvent.getSource().getBindingContext();
				var oView = this.getView().getParent();
				var oDetails = {};
				oDetails.Origin = oView.byId("origin").getText();
				oDetails.IdSap = oView.byId("idsap").getText();
				oDetails.Voorach = oView.byId("voorach").getText();
				oDetails.Signi = oView.byId("kenteken").getValue();
				oDetails.Lgpla = oView.byId("locatie").getValue();
				oDetails.Waste = oView.byId("waste").getValue();
				oDetails.ContNumber = oView.byId("container").getValue();
				oDetails.WasteDesc = oView.byId("wastedesc").getText();
				oDetails.Lbert = oView.byId("locatietekst").getText();
				oDetails.Name1 = oView.byId("transporteur").getText();
				oDetails.Milieuclass = oView.byId("mtype").getUnit();
				oDetails.Charg = oView.byId("socharge").getText();
				
				// oDetails = {};
				// oDetails.Origin = oView.byId("origin").getText();
				// oDetails.IdSap = oView.byId("idsap").getText();
				// oDetails.Voorach = oView.byId("Voorach123").getText();
				// oDetails.Signi = oView.byId("kenteken").getValue();
				// oDetails.Lgpla = oView.byId("locatie").getValue();
				// oDetails.Waste = oView.byId("waste").getValue();
				// oDetails.ContNumber = oView.byId("container").getValue();
				// oDetails.WasteDesc = oView.byId("wastedesc").getText();
				// oDetails.Name1 = oView.byId("transporteur").getText();
				// oDetails.Milieuclass = oView.byId("mtype").getUnit();

				oView = this.getView().getParent();

				oEntry = {};
				//				oEntry.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
				oEntry.Objnr = oBindingContext.getProperty("Objnr");
				oEntry.ObjectGrp = oBindingContext.getProperty("ObjectGrp");
				oEntry.FleetCat = oBindingContext.getProperty("FleetCat");
				oEntry.Voorach = oBindingContext.getProperty("Voorach");
				oEntry.Origin = oBindingContext.getProperty("Origin");
				oEntry.IdSap = oBindingContext.getProperty("IdSap");
				oEntry.Waste = oView.byId("waste").getValue();
				oEntry.TransnrI = oBindingContext.getProperty("TransnrI");
				oEntry.WasteDesc = oBindingContext.getProperty("WasteDesc");
				oEntry.Charg = oBindingContext.getProperty("Charg");
				oEntry.Verif = oBindingContext.getProperty("Verif");
				oEntry.WdfDescription = oBindingContext.getProperty("WdfDescription");
				oEntry.LicenseNum = oView.byId("kenteken").getValue();
				oEntry.ContNumber = oView.byId("container").getValue();
				oEntry.Lbert = oView.byId("locatietekst").getText();
				oEntry.Lgpla = oView.byId("locatie").getValue();
				oEntry.ContractPosnr = oBindingContext.getProperty("ContractPosnr");
				oEntry.Approved = 'X';

				var oModel = this.getOwnerComponent().getModel();
				oModel.update("/UpdateDetailsSet(Origin='" + oDetails.Origin + "',IdSap='" + oDetails.IdSap + "')", oDetails, {
					method: "PUT",
					success: function(data) {},
					error: function(e) {
						// lert("error");
					}
				});
				
				if (oModelEmma.oData.GvEmma !== "ZMIN") {
					
					sRoot = "/SaveEndSet(Origin='" + oBindingContext.getProperty("Origin") + "',IdSap='" + oBindingContext.getProperty("IdSap") +
						"',Voorach='" + oBindingContext.getProperty("Voorach") + "')";
					var sURL = sRoot;

					while (sRoot.search("/") !== -1) {
						sRoot = sRoot.replace("/", "%2F");
					}

					var oModelOdata = this.getOwnerComponent().getModel();

					oModelOdata.setUseBatch(false);

					oModelOdata.update(sURL, oEntry, {
						method: "PUT",
						success: function() {
							//	sap.ui.commons.MessageBox.show(
							//		sap.ui.commons.MessageBox.alert("Success!")
							//		);
						},
						error: function() {
							//	sap.ui.commons.MessageBox.alert("Error!");
						}
					});
				}
				
				oCasenrInsp = sap.ui.getCore().getModel("jsonModelEmmaCase").getProperty("/emmacase/d/CasenrInsp");

				oUploadCollection = this.getView().byId("UploadCollection");
				cFiles = oUploadCollection.getItems().length;
				resourcemodel = this.getOwnerComponent().getModel();
				oDataResource = {
					sServiceUrl: resourcemodel.sServiceUrl + "/FileSet"
				};
				var jsonResource = new sap.ui.model.json.JSONModel(oDataResource);
				this.getView().setModel(jsonResource, "ResourceModel");
				if (cFiles > 0) {
					oUploadCollection = this.getView().byId("UploadCollection");
					var sServiceUrl = resourcemodel.sServiceUrl + "/FileSet";
					for (var i = 0; i < oUploadCollection._aFileUploadersForPendingUpload.length; i++) {
						var oCustomerHeaderSlug = new sap.ui.unified.FileUploaderParameter({
							name: "slug",
							value: oCasenrInsp + "_0" + i + ".jpg"
						});
						oUploadCollection.NewModel = window.location.origin + "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/FileSet";
						sServiceUrl = window.location.origin + "/sap/opu/odata/sap/ZMOB_CONTRAPP_SRV/FileSet";
						oUploadCollection._aFileUploadersForPendingUpload[i].setUploadUrl(sServiceUrl);
						oUploadCollection._aFileUploadersForPendingUpload[i].addHeaderParameter(oCustomerHeaderSlug);
					}
					oUploadCollection.upload();
					oUploadCollection._aFileUploadersForPendingUpload = [];
				}
			var oView = this.getView();

			var oModel = this.getOwnerComponent().getModel();

			var oControl = oView.byId("table");
			var caseinsp = sap.ui.getCore().getModel("jsonModelEmmaCase").getProperty("/emmacase/d/CasenrInsp");
			var oList = oControl.mBindingInfos.items.binding.oList;
			//			var oSelected = oView.bAllowTextSelection;
			for (var i = 0; i < oList.length; i++) {
				var pSelected = oList[i].Checked;
				if (pSelected === true) {
					var nodeID = oList[i].NodeId;
					if (pSelected === true) {
						oModel = this.getOwnerComponent().getModel();
						var oEntry = {};
						oEntry.CasenrInsp = caseinsp;
						oEntry.NodeId = nodeID;
						oModel.setUseBatch(false);
						// "/SaveRejectReasonsSet(CasenrInsp='" + oEntry.CasenrInsp + "',NodeId='" + oEntry.NodeId + "')"
						oModel.update("/SaveRejectReasonsSet(CasenrInsp='" + oEntry.CasenrInsp + "',NodeId='" + oEntry.NodeId + "')", oEntry, {
							success: function(data) {},
							error: function(e) {
								// lert("error");
							}
						});
					}
				}
			}
				
				if (oModelEmma.oData.GvEmma !== "ZMIP") {
					this._afwijzingRedenenen(oCasenrInsp);
				}

				if (oModelEmma.oData.GvEmma !== "ZMIN") {

					oView = this.getView().getParent();
					oBindingContext = oView.getBindingContext();
					oEntry = {};
					oEntry.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
					oEntry.Objnr = oBindingContext.getProperty("Objnr");
					oEntry.ObjectGrp = oBindingContext.getProperty("ObjectGrp");
					oEntry.FleetCat = oBindingContext.getProperty("FleetCat");
					oEntry.Voorach = oBindingContext.getProperty("Voorach");
					oEntry.Verif = oBindingContext.getProperty("Verif");
					oEntry.WdfDescription = oBindingContext.getProperty("WdfDescription");
					oEntry.LicenseNum = oView.byId("kenteken").getValue();
					oEntry.Lbert = oView.byId("locatietekst").getText();
					oEntry.Lgpla = fcontainer;
					oEntry.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
					oEntry.ContractPosnr = oBindingContext.getProperty("ContractPosnr");
					oEntry.Reason = oBindingContext.getProperty("InspectieText");
					oEntry.Approved = 'X';
					if (oBindingContext.getProperty("InspectieText") === "UITGESTELD") {
						oEntry.Reason = "UITEMMA";
					} else {
						oEntry.Reason = oBindingContext.getProperty("InspectieText");
					}
					var that = this;
					oModel = that.getOwnerComponent().getModel();
					oModel.setUseBatch(false);
					sURL = "/UpdateDetailsSet(Origin='" + oBindingContext.getProperty("Origin") + "',IdSap='" + oBindingContext.getProperty(
						"IdSap") + "')";
					oModel.update(sURL, oDetails, {
						method: "PUT",
						success: function(data) {
							sURL = "/SaveEndSet(Origin='" + oBindingContext.getProperty("Origin") + "',IdSap='" + oBindingContext.getProperty("IdSap") +
								"',Voorach='" + oBindingContext.getProperty("Voorach") + "')";
							oModelOdata = that.getOwnerComponent().getModel();
							oModelOdata.setUseBatch(false);
							// oModelOdata.setUseBatch(true);
							oModelOdata.update(sURL, oEntry, {
								//					method: "PUT",
								success: function() {
			
										that.doNavigate("Transport", "", "", "");
										window.location.reload();
										
								},
								error: function() {
									sap.ui.commons.MessageBox.alert("Error!");
								}
							});

						},
						error: function(e) {
							// lert("error");
						}
					});
				}else{
					this.doNavigate("Transport", "", "", "");
					window.location.reload();
				}
			}
			oBindingContext = oEvent.getSource().getBindingContext();
			// return new Promise(function(fnResolve) {

			// 	this.doNavigate("Transport", oBindingContext, fnResolve, "");
			// 	window.location.reload();
			// }.bind(this)).catch(function(err) {
			// 	if (err !== undefined) {
			// 		MessageBox.error(err.message);
			// 	}
			// });
		},

		Derefresh: function() {
			this.window.location.reload();

		},
		updateHomeset: function(oEvent) {
			var oView = this.getView().getParent();
			var oBindingContext = oEvent.getSource().getBindingContext();
			var oDetails = {};
//			oDetails.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
			oDetails.Origin = oView.byId("origin").getText();
			oDetails.IdSap = oView.byId("idsap").getText();
			oDetails.Voorach = oView.byId("voorach").getText();
			oDetails.Signi = oView.byId("kenteken").getValue();
			oDetails.Lgpla = oView.byId("locatie").getValue();
			oDetails.Waste = oView.byId("waste").getValue();
			oDetails.ContNumber = oView.byId("container").getValue();
			oDetails.WasteDesc = oView.byId("wastedesc").getText();
			oDetails.Lbert = oView.byId("locatietekst").getText();
			oDetails.Name1 = oView.byId("transporteur").getText();
			oDetails.Milieuclass = oView.byId("mtype").getUnit();
			oDetails.Charg = oView.byId("socharge").getText();



			var oEntry = {};
			oEntry.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
			oEntry.Objnr = oBindingContext.getProperty("Objnr");
			oEntry.ObjectGrp = oBindingContext.getProperty("ObjectGrp");
			oEntry.FleetCat = oBindingContext.getProperty("FleetCat");
			oEntry.Voorach = oBindingContext.getProperty("Voorach");
			oEntry.Origin = oBindingContext.getProperty("Origin");
			oEntry.IdSap = oBindingContext.getProperty("IdSap");
			oEntry.Waste = oBindingContext.getProperty("Waste");
			oEntry.TransnrI = oBindingContext.getProperty("TransnrI");
			oEntry.WasteDesc = oBindingContext.getProperty("WasteDesc");
			oEntry.Charg = oBindingContext.getProperty("Charg");
			oEntry.Verif = oBindingContext.getProperty("Verif");
			oEntry.WdfDescription = oBindingContext.getProperty("WdfDescription");
			oEntry.LicenseNum = oBindingContext.getProperty("LicenseNum");
			oEntry.ContNumber = oBindingContext.getProperty("ContNumber");
			oEntry.Lbert = oView.byId("locatietekst").getText();
			oEntry.Lgpla = oBindingContext.getProperty("Lgpla");
			oEntry.ContractPosnr = oBindingContext.getProperty("ContractPosnr");

			// var oEntry2 = {};
			// oEntry2.ContNumber = oBindingContext.getProperty("ContNumber");
			// oEntry2.Soort = oBindingContext.getProperty("Soort");
			// oEntry2.Contract = oBindingContext.getProperty("Contract");
			// oEntry2.Transporter = oBindingContext.getProperty("Transporter");
			// oEntry2.Waste = oBindingContext.getProperty("Waste");
			// oEntry2.Lgtyp = oBindingContext.getProperty("Lgtyp");
			// oEntry2.Voorach = oBindingContext.getProperty("Voorach");
			// oEntry2.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
			// oEntry2.Objnr = oBindingContext.getProperty("Objnr");
			// oEntry2.ObjectGrp = oBindingContext.getProperty("ObjectGrp");
			// oEntry2.FleetCat = oBindingContext.getProperty("FleetCat");
			// oEntry2.LicenseNum = oBindingContext.getProperty("LicenseNum");

			oView = this.getView().getParent();
			oDetails = {};

//			oDetails.Wdplantnr = oBindingContext.getProperty("Wdplantnr");
			oDetails.Origin = oView.byId("origin").getText();
			oDetails.IdSap = oView.byId("idsap").getText();
			oDetails.Voorach = oView.byId("Voorach123").getText();
			oDetails.Signi = oView.byId("kenteken").getValue();
			oDetails.Lgpla = oView.byId("locatie").getValue();
			oDetails.Waste = oView.byId("waste").getValue();
			oDetails.ContNumber = oView.byId("container").getValue();
			oDetails.WasteDesc = oView.byId("wastedesc").getText();
			oDetails.Lbert = oView.byId("locatietekst").getText();
			oDetails.Name1 = oView.byId("transporteur").getText();
			oDetails.Milieuclass = oView.byId("mtype").getUnit();
			oDetails.Charg = oView.byId("socharge").getText();

			var oModel = this.getOwnerComponent().getModel();
			oModel.update("/UpdateDetailsSet(Origin='" + oDetails.Origin + "',IdSap='" + oDetails.IdSap + "')", oDetails, {
				method: "PUT",
				success: function(data) {},
				error: function(e) {
					// lert("error");
				}
			});

			var sRoot = "/SaveEndSet(Origin='" + oBindingContext.getProperty("Origin") + "',IdSap='" + oBindingContext.getProperty("IdSap") +
				"',Voorach='" + oBindingContext.getProperty("Voorach") + "')";
			var sURL = sRoot;

			while (sRoot.search("/") !== -1) {
				sRoot = sRoot.replace("/", "%2F");
			}

			//	sURL = "/" + sRoot;

			var oModelOdata = this.getOwnerComponent().getModel();

			oModelOdata.setUseBatch(false);

			oModelOdata.update(sURL, oEntry, {
				method: "PUT",
				success: function() {
					//	sap.ui.commons.MessageBox.show(
					//		sap.ui.commons.MessageBox.alert("Success!")
					//		);
				},
				error: function() {
					//	sap.ui.commons.MessageBox.alert("Error!");
				}
			});

		},

		wait: function(ms) {
			var start = new Date().getTime();
			var end = start;
			while (end < start + ms) {
				end = new Date().getTime();
			}
		},

		_afwijzingRedenenen: function(vCasenrInsp) {
			var oView = this.getView();

			var oModel = this.getOwnerComponent().getModel();

			var oControl = oView.byId("table");

			var oList = oControl.mBindingInfos.items.binding.oList;
			//			var oSelected = oView.bAllowTextSelection;
			for (var i = 0; i < oList.length; i++) {
				var pSelected = oList[i].Checked;
				if (pSelected === true) {
					var nodeID = oList[i].NodeId;
					if (pSelected === true) {
						oModel = this.getOwnerComponent().getModel();
						var oEntry = {};
						oEntry.CasenrInsp = vCasenrInsp;
						oEntry.NodeId = nodeID;
						oModel.setUseBatch(false);
						// "/SaveRejectReasonsSet(CasenrInsp='" + oEntry.CasenrInsp + "',NodeId='" + oEntry.NodeId + "')"
						oModel.update("/SaveRejectReasonsSet(CasenrInsp='" + oEntry.CasenrInsp + "',NodeId='" + oEntry.NodeId + "')", oEntry, {
							success: function(data) {},
							error: function(e) {
								// lert("error");
							}
						});
					}
				}
			}

		},

		_onButtonPressUpload: function() {
			var oUploadCollection = this.getView().byId("UploadCollection");
			var cFiles = oUploadCollection.getItems().length;
			//			var uploadInfo = cFiles + " file(s)";
			//			var resourcemodel = this.getOwnerComponent().getModel();
			var oDialog = this.getView().getContent()[0];
			//			var oDataResource = {
			//				sServiceUrl: resourcemodel.sServiceUrl + "/FileSet"
			//			};
			if (cFiles > 4) {
				sap.m.MessageToast.show();
			} else if (cFiles < 3) {
				sap.m.MessageToast.show();
			} else {
				oDialog.close();
			}
		},

		_onCancel: function() {
			var oDialog = this.getView().getContent()[0];

			return new Promise(function(fnResolve) {
				oDialog.attachEventOnce("afterClose", null, fnResolve);
				oDialog.close();
			});

		},
		_onSearchFieldSearch3: function(oEvent) {
			var sControlId = "table";
			var oControl = this.getView().byId(sControlId);
			// Get the search query, regardless of the triggered event ('query' for the search event, 'newValue' for the liveChange one).
			var sQuery = oEvent.getParameter("query") || oEvent.getParameter("newValue");
			var sSourceId = oEvent.getSource().getId();

			return new Promise(function(fnResolve) {
				var aFinalFilters = [];

				var aFilters = [];
				// 1) Search filters (with OR)
				if (sQuery && sQuery.length > 0) {

					aFilters.push(new sap.ui.model.Filter("RejText", sap.ui.model.FilterOperator.Contains, sQuery));

				}

				var aFinalFilters = aFilters.length > 0 ? [new sap.ui.model.Filter(aFilters, false)] : [];
				var oBindingOptions = this.updateBindingOptions(sControlId, {
					filters: aFinalFilters
				}, sSourceId);
				var oBindingInfo = oControl.getBindingInfo("items");
				oControl.bindAggregation("items", {
					model: oBindingInfo.model,
					path: oBindingInfo.path,
					parameters: oBindingInfo.parameters,
					template: oBindingInfo.template,
					sorter: oBindingOptions.sorters,
					filters: oBindingOptions.filters
				});
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});
		},

		onInit: function() {
			this.mBindingOptions = {};
			this._oDialog = this.getView().getContent()[0];

			// var oModel1 = sap.ui.getCore().getModel("EmmaCase");
			// this.getView().byId("TheForm").setModel(oModel1);

			// var oModel2 = sap.ui.getCore().getModel("EmmaCase");
			// this.getView().byId("Casetxt").setModel(oModel2);

			var oModelEmma = sap.ui.getCore().getModel("jsonModelEmma");
			this.getView().byId("UploadCollection").setModel(oModelEmma);

			var oTreeModel = sap.ui.getCore().getModel("jsonTreeModel");

			this.getView().byId("table").setModel(oTreeModel);

			this._oFacetFilter = null;

		},

		_filter: function() {
			var oFilter = null;
			oFilter = this._oFacetFilter;
			this.getView().byId("table").getBinding("rows").filter(oFilter, "Application");
		},

		clearAllFilters: function() {
			this.handleFacetFilterReset();
			this._filter();
		},

		_getFacetFilterLists: function() {
			var oFacetFilter = this.getView().byId("facetFilter");
			return oFacetFilter.getLists();
		},

		handleFacetFilterReset: function(oEvent) {
			var aFacetFilterLists = this._getFacetFilterLists();

			for (var i = 0; i < aFacetFilterLists.length; i++) {
				for (var i = 0; i < aFacetFilterLists.length; i++) {
					aFacetFilterLists[i].setSelectedKeys();
				}
			}
			this._oFacetFilter = null;

			if (oEvent) {
				this._filter();
			}
		},

		handleListClose: function(oEvent) {
			var aFacetFilterLists = this._getFacetFilterLists().filter(function(oList) {
				return oList.getActive() && oList.getSelectedItems().length;
			});

			this._oFacetFilter = new Filter(aFacetFilterLists.map(function(oList) {
				return new Filter(oList.getSelectedItems().map(function(oItem) {
					return new Filter(oList.getTitle(), FilterOperator.EQ, oItem.getText());
				}), false);
			}), true);

			this._filter();
		},
		afterOpen: function() {

			var oModel1 = sap.ui.getCore().getModel("EmmaCase");
			this.getView().byId("TheForm").setModel(oModel1);

			var oModel2 = sap.ui.getCore().getModel("EmmaCase");
			this.getView().byId("Casetxt").setModel(oModel2);

			var oModelEmma = sap.ui.getCore().getModel("jsonModelEmma");
			this.getView().byId("UploadCollection").setModel(oModelEmma);

			/* for the tree */
			var oModel = new sap.ui.model.json.JSONModel();
			oModel.loadData("../localService/Tree.json");
			this.getView().byId("Tree").setModel(oModel);

		},
		
		_onListSelectionChange: function(oEvent) {

			var oDialog = this.getView().getContent()[0];

			var oView = this.getView().getParent();
			var oKenteken = oView.byId("kenteken");
			var oType = oView.byId("mtype");
			
			var oItem = this.getView();
			oItem = oEvent.getParameter("listItem");
			var oCtx = oItem.getBindingContext();
			var sKenteken = oCtx.getProperty("Kenteken");
			var sType = oCtx.getProperty("Type");
			
			oKenteken.setValue(sKenteken);
			oType.setUnit(sType);

			return new Promise(function(fnResolve) {
				oDialog.attachEventOnce("afterClose", null, fnResolve);
				oDialog.close();

			});

		},
		
		updateBindingOptions: function(sCollectionId, oBindingData, sSourceId) {
			this.mBindingOptions[sCollectionId] = this.mBindingOptions[sCollectionId] || {};

			var aSorters = oBindingData.sorters === undefined ? this.mBindingOptions[sCollectionId].sorters : oBindingData.sorters;
			var oGroupby = oBindingData.groupby === undefined ? this.mBindingOptions[sCollectionId].groupby : oBindingData.groupby;

			// 1) Update the filters map for the given collection and source
			this.mBindingOptions[sCollectionId].sorters = aSorters;
			this.mBindingOptions[sCollectionId].groupby = oGroupby;
			this.mBindingOptions[sCollectionId].filters = this.mBindingOptions[sCollectionId].filters || {};
			this.mBindingOptions[sCollectionId].filters[sSourceId] = oBindingData.filters || [];

			// 2) Reapply all the filters and sorters
			var aFilters = [];
			for (var key in this.mBindingOptions[sCollectionId].filters) {
				aFilters = aFilters.concat(this.mBindingOptions[sCollectionId].filters[key]);
			}

			// Add the groupby first in the sorters array
			if (oGroupby) {
				aSorters = aSorters ? [oGroupby].concat(aSorters) : [oGroupby];
			}

			var aFinalFilters = aFilters.length > 0 ? [new sap.ui.model.Filter(aFilters, true)] : undefined;
			return {
				filters: aFinalFilters,
				sorters: aSorters
			};

		},

		onExit: function() {
			this._oDialog.destroy();

		}
	});
}, /* bExport= */ true);