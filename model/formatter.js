sap.ui.define([], function() {
	"use strict";

	return {

		setEmmaSearchIconVisible: function(emma) {
			//is numeric?
			if (!isNaN(parseFloat(emma)) && isFinite(emma)) {
				return true;
			} else {
				return false;
			}
		},

		setInspectieColor: function(inspectie) {

		
			switch (inspectie) {
				case "Hoog":
					return "Error";
				case "Laag":
					return "Success";
				case "Uitgesteld":
					return "None";
				case "":
					return "None";
				default:
					return "None";
			}
		
		
			
		},
		
		setEUROColor: function(Milieuclass) {
		   if(Milieuclass == "EURO_1") {
		      return "Error"; 
	    }
		    else if (Milieuclass == "EURO_2") {
		
		        return "Error";
		    }
		    else if (Milieuclass == "EURO_3") {
		        return "Error";
		    }
		    else
		    {
		        return "Success"; 
		    }
		}
		
	};

});